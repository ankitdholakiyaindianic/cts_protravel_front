var ModuleName = 'ctsApp';
var App = angular.module(ModuleName, [
    "ui.router",
    "route",
    "ngCookies",
    "oc.lazyLoad",
    "constants",
    "commonDirectives",
    "defaultServices",
    "MainController",
    "ngMaterial",
    "md.data.table",
    "ngAnimate",
    "ngMessages",
    "authenticationServices",
    "user.api.services",
    'toaster',
    "ngIdle"
    ]);

App.config(['IdleProvider', 'KeepaliveProvider', function(IdleProvider, KeepaliveProvider) {
    IdleProvider.idle(300);
    IdleProvider.timeout(900);
    KeepaliveProvider.interval(10);
}]);

App.run(['Idle', function(Idle) {
    Idle.watch();
}]);

App.run(['$rootScope', '$state', '$stateParams', '$cookieStore', '$cookies', 'authService',
    function($rootScope, $state, $stateParams, $cookieStore, $cookies, authService) {

        /* ===== Access Page left menu ====== */
        var returnObjAccess = function(val) {
            return function(element) {
                if(val.web_menus && element.menu_id === val.menu_id && element.web_menus === val.web_menus){
                    return element;    
                }else if(element.report_id === val.report_id && element.web_menus === val.web_menus){
                    return element;    
                }
            }
        }
        var checkAccess = function(obj){

            if(sessionStorage.getItem('userInfo')){
                var userInfo = JSON.parse(sessionStorage.getItem('userInfo'));
                
                if(userInfo && userInfo.superUser == true){
                    return true;
                }
                else{

                    if(userInfo && userInfo.UserPermissionsResponseViewModel){
                        var validPage = userInfo.UserPermissionsResponseViewModel.filter(returnObjAccess(obj));
                        if(validPage.length){
                            return true;
                        }
                        return false;
                    }
                }
            }

        }
        /* ===== Access Page left menu ====== */

        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {

            if (toState.name.indexOf('main') > -1 && authService.isAuthenticated() == '') {
                event.preventDefault();
                $state.go('login');
                return;
            } else if (toState.name == 'main' && authService.isAuthenticated() != '') {
                event.preventDefault();
                $state.go('main.home');
                return;
            } else if (toState.name.indexOf('login') > -1 && authService.isAuthenticated() != '') {
                event.preventDefault();
                $state.go('main.home');
                return;
            } else if(toState.data.accessAllow){                
                var callAC = checkAccess(toState.data.accessAllow);
                if(!callAC){
                    event.preventDefault();
                    $state.go('main.home');
                    return;
                }
            }
        });
    }
    ]);

App.config(['$controllerProvider', function($controllerProvider) {
    // this option might be handy for migrating old apps, but please don't use it in new ones!
    $controllerProvider.allowGlobals();
}]);

App.config(['$stateProvider', '$urlRouterProvider', '$locationProvider','$httpProvider','GLOBALS','SETTINGS', function($stateProvider, $urlRouterProvider, $locationProvider,$httpProvider,GLOBALS,SETTINGS) {

    $urlRouterProvider.otherwise( function($injector, $location) {
        var $state = $injector.get('$state');
        var Storage = $injector.get('authService');
        if (Storage.isAuthenticated()) {
            $state.go('main.home');
        }
        else {
            $state.go('login');
        }
    });
    if (history.pushState) {
        $locationProvider.hashPrefix('!');
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: true
        });
    } else {        
        $locationProvider.hashPrefix('!');
        $locationProvider.html5Mode({
            enabled: false,
            requireBase: false
        });
    }
    $httpProvider.interceptors.push('resourceinterceptor');
    $httpProvider.interceptors.push('interceptHttp');

}]);

/* Init global settings and run the app */
App.run(["$rootScope", "$state","trafficCop", function($rootScope, $state, trafficCop) {
    $rootScope.$state = $state; // state to be accessed from view 
    $rootScope.trafficCop = trafficCop;
}]);

App.run(['$templateCache', function ( $templateCache ) {
    console.log("*");
    $templateCache.remove();
    console.log("^");
}]);

angular.element(document).ready(function() {
  angular.bootstrap(document, [ModuleName]);
});

function setMetaData($scope, $rootScope, MetaInformation, seo) {
    if (typeof seo != "undefined") {
        if (typeof seo.seo_title != "undefined") {
            MetaInformation.setMetaTitle(seo.seo_title);
        }
        if (typeof seo.seo_description != "undefined") {
            MetaInformation.setMetaDescription(seo.seo_description)
        }
        if (typeof seo.seo_keywords != "undefined") {
            MetaInformation.setMetaKeyword(seo.seo_keywords)
        }
        $rootScope.metaData = MetaInformation;
    }
}

function spinnerPos() {
    var windowHeight = $(window).height();
    var windowWidth = $(window).width();
    var spinnerHeight = 120;
    var spinnerWidth = 120;
    var spleft= (windowWidth - spinnerWidth) / 2;
    var sptop= (windowHeight - spinnerHeight) / 2
    $('.loading-container').css({
        'left': spleft,
        'top': sptop
    })
}