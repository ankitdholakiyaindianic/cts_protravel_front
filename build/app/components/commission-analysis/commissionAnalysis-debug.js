App.controller('commissionAnalysisCtrl', ['$scope','$state','$stateParams', '$rootScope', 'MetaInformation','$http', 'SETTINGS', 'GLOBALS', 'authService','toaster','$mdDialog','$filter', function($scope,$state,$stateParams, $rootScope, MetaInformation,$http, SETTINGS, GLOBALS, authService,toaster,$mdDialog,$filter) {

    $scope.unmatchedPaymentRpt = {};
    $scope.unmatchedPaymentRpt.fromDate = null;
    $scope.unmatchedPaymentRpt.toDate = null;
    $scope.unmatchedPaymentRpt.selectedDate = 0;

    $scope.noData = "No data found";
    $scope.corpID = $scope.userDetails.corpID;
    $scope.unmatchedPaymentLoading = false;

    /* ==== pagination options ==== */
    $scope.limitOptions = [10,25,50];
    $scope.query = {};
    $scope.query.limit = 10;
    $scope.query.page = 1;

    var finalTotal = 0;
    $scope.totalCommission = function(obj){
        finalTotal += Number(obj.net_act_comm);
        return finalTotal;
    }
    function filterDate(obj){
        if(obj.deposit_date){
            obj.deposit_date = new Date(obj.deposit_date);
        }
        if(obj.check_date){
            obj.check_date = new Date(obj.check_date);
        }
        return obj;
    }

    $scope.bookingToFilter = function() {
        indexedTeams = [];
        return $scope.unmatchedPayment;
    }


    $scope.filterTeams = function(player) {
        var teamIsNew = indexedTeams.indexOf(player.Type) == -1;
        if (teamIsNew) {
            indexedTeams.push(player.Type);
        }
        return teamIsNew;
    }

    $scope.bookingReportParms = {
        dataTableCommon:{
            param:{
                Draw:1,
                Start:0,
                Length:10
            },
            ExportType:0
        },
        corpID:$scope.corpID,
        fromDate: ($scope.unmatchedPaymentRpt.fromDate) ? $scope.unmatchedPaymentRpt.fromDate : "",
        toDate: ($scope.unmatchedPaymentRpt.toDate) ? $scope.unmatchedPaymentRpt.toDate : "",
    };

    /* ===== CallBack Global Left panel View ===== */
    $scope.copyObj = angular.copy($scope.bookingReportParms);
    
    $scope.$emit('callSelectedFieldByPage',$scope.bookingReportParms);

    $scope.$on('callSelectedField', function(evnt,obj) { 
        var finalObj = angular.copy($scope.copyObj);
        // $scope.bookingReportParms = Object.assign(finalObj,obj); 
        $scope.bookingReportParms = $.extend(finalObj, obj);
        $scope.commisAnays.$valid = true;
        $scope.callFiterData(true);
    });


    $scope.checkValidDate = function(fromDate,toDate){
        if(toDate && fromDate > toDate){
            $scope.unmatchedPaymentRpt.toDate = fromDate;
        }
    }

// $scope.unmatchedPayment = [{"Type":1,"PropertyName":"HOTEL METROPOLE MONTE CAR","PropertyCity":"","PropertyState":"","Bookings":1,"PaymentCount":0,"ExpectedCommission":0.0,"ActualCommission":0.0,"Pmt":0,"PaymentDays":0,"CXL":0,"DUE":0,"NC":0,"PP":0,"NS":0,"UnAns":0,"Other":0,"UnColl":0,"typeDescription":"Hotel","roomNights":1,"paymentPercent":0.0,"resolvedPercent":0.0,"commissionPercent":0.0,"averagePaymentDays":0.0},{"Type":1,"PropertyName":"THE LONDON NYC","PropertyCity":"NEW YORK","PropertyState":"","Bookings":3,"PaymentCount":0,"ExpectedCommission":0.0,"ActualCommission":0.0,"Pmt":0,"PaymentDays":0,"CXL":0,"DUE":0,"NC":0,"PP":0,"NS":0,"UnAns":0,"Other":0,"UnColl":0,"typeDescription":"Hotel","roomNights":6,"paymentPercent":0.0,"resolvedPercent":0.0,"commissionPercent":0.0,"averagePaymentDays":0.0},{"Type":1,"PropertyName":"HOTEL D ANGLETERRE","PropertyCity":"","PropertyState":"","Bookings":2,"PaymentCount":0,"ExpectedCommission":0.0,"ActualCommission":0.0,"Pmt":0,"PaymentDays":0,"CXL":0,"DUE":0,"NC":0,"PP":0,"NS":0,"UnAns":0,"Other":0,"UnColl":0,"typeDescription":"Hotel","roomNights":2,"paymentPercent":0.0,"resolvedPercent":0.0,"commissionPercent":0.0,"averagePaymentDays":0.0},{"Type":1,"PropertyName":"BABUINO 181","PropertyCity":"","PropertyState":"","Bookings":6,"PaymentCount":0,"ExpectedCommission":0.0,"ActualCommission":0.0,"Pmt":0,"PaymentDays":0,"CXL":0,"DUE":0,"NC":0,"PP":0,"NS":0,"UnAns":0,"Other":1,"UnColl":0,"typeDescription":"Hotel","roomNights":13,"paymentPercent":0.0,"resolvedPercent":0.0,"commissionPercent":0.0,"averagePaymentDays":0.0},{"Type":1,"PropertyName":"HOTEL MARLOWE","PropertyCity":"Cambridge","PropertyState":"","Bookings":3,"PaymentCount":0,"ExpectedCommission":0.0,"ActualCommission":0.0,"Pmt":0,"PaymentDays":0,"CXL":0,"DUE":0,"NC":0,"PP":0,"NS":0,"UnAns":0,"Other":0,"UnColl":0,"typeDescription":"Hotel","roomNights":3,"paymentPercent":0.0,"resolvedPercent":0.0,"commissionPercent":0.0,"averagePaymentDays":0.0},{"Type":1,"PropertyName":"CLAYTON HOTEL","PropertyCity":"LONDON","PropertyState":"","Bookings":3,"PaymentCount":0,"ExpectedCommission":0.0,"ActualCommission":0.0,"Pmt":0,"PaymentDays":0,"CXL":0,"DUE":0,"NC":0,"PP":0,"NS":0,"UnAns":0,"Other":0,"UnColl":0,"typeDescription":"Hotel","roomNights":3,"paymentPercent":0.0,"resolvedPercent":0.0,"commissionPercent":0.0,"averagePaymentDays":0.0},{"Type":1,"PropertyName":"1001 3RD STREET","PropertyCity":"West Hollywood","PropertyState":"","Bookings":2,"PaymentCount":0,"ExpectedCommission":0.0,"ActualCommission":0.0,"Pmt":0,"PaymentDays":0,"CXL":0,"DUE":0,"NC":0,"PP":0,"NS":0,"UnAns":0,"Other":0,"UnColl":0,"typeDescription":"Hotel","roomNights":2,"paymentPercent":0.0,"resolvedPercent":0.0,"commissionPercent":0.0,"averagePaymentDays":0.0},{"Type":1,"PropertyName":"HYATT HOTELS","PropertyCity":"Toronto","PropertyState":"","Bookings":2,"PaymentCount":0,"ExpectedCommission":0.0,"ActualCommission":0.0,"Pmt":0,"PaymentDays":0,"CXL":0,"DUE":0,"NC":0,"PP":0,"NS":1,"UnAns":0,"Other":0,"UnColl":0,"typeDescription":"Hotel","roomNights":2,"paymentPercent":0.0,"resolvedPercent":0.0,"commissionPercent":0.0,"averagePaymentDays":0.0},{"Type":1,"PropertyName":"RAFFLES INT HOTEL","PropertyCity":"DUBAI","PropertyState":"","Bookings":4,"PaymentCount":0,"ExpectedCommission":0.0,"ActualCommission":0.0,"Pmt":0,"PaymentDays":0,"CXL":0,"DUE":0,"NC":0,"PP":0,"NS":0,"UnAns":0,"Other":0,"UnColl":0,"typeDescription":"Hotel","roomNights":4,"paymentPercent":0.0,"resolvedPercent":0.0,"commissionPercent":0.0,"averagePaymentDays":0.0},{"Type":1,"PropertyName":"W MONTREAL","PropertyCity":"MONTREAL","PropertyState":"","Bookings":6,"PaymentCount":0,"ExpectedCommission":0.0,"ActualCommission":0.0,"Pmt":0,"PaymentDays":0,"CXL":0,"DUE":0,"NC":0,"PP":0,"NS":0,"UnAns":0,"Other":0,"UnColl":0,"typeDescription":"Hotel","roomNights":21,"paymentPercent":0.0,"resolvedPercent":0.0,"commissionPercent":0.0,"averagePaymentDays":0.0}]

$scope.callFiterData = function(valid){
    if($stateParams.fromdate && $stateParams.todate || $scope.commisAnays.$valid){
        if(valid){
            $scope.bookingReportParms.dataTableCommon.ExportType = 0;
            $scope.bookingReportParms.dataTableCommon.param.Start = 0;
            $scope.bookingReportParms.dataTableCommon.param.Length = 10;
            if($scope.bookingReportParms.dataTableCommon.param.Columns){
                delete $scope.bookingReportParms.dataTableCommon.param.Columns;
            }
        }
        if(!$scope.bookingReportParms.dataTableCommon.ExportType){
            $scope.unmatchedPayment = [];
            $scope.bookingReportParms.fromDate = $filter('date')($scope.unmatchedPaymentRpt.fromDate, "dd/MM/yyyy");
            $scope.bookingReportParms.toDate = $filter('date')($scope.unmatchedPaymentRpt.toDate, "dd/MM/yyyy");
            $scope.bookingReportParms.dateSelect = $scope.unmatchedPaymentRpt.selectedDate;
            // $scope.bookingReportParms.dateSelect = $scope.unmatchedPaymentRpt.selectedDate;
            $scope.unmatchedPaymentLoading = true;
            $scope.$emit('callSelectedFieldByPage',$scope.bookingReportParms);
            if($scope.bookingReportParms.pageStart){
                $scope.query.page = 1;
            }
        }
        
        $http({
            url: SETTINGS[GLOBALS.ENV].apiUrl+"Commission/GetCommissionAnalysis",
            method: 'POST',
            data: $scope.bookingReportParms,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
            }
        }).success(function(response) {
            $scope.unmatchedPaymentLoading = false;
            if(response.ResposeCode == 2){
                if(response.DataList.data){
                    $scope.unmatchedPayment = response.DataList.data;
                }
                if(response.DataList.fileName){
                    /*window.open(
                        SETTINGS[GLOBALS.ENV].apiUrl+response.DataList.fileName,
                        '_blank'
                        );*/
                        window.open(
                            response.DataList.fileName,
                            '_blank'
                            );
                    }
                    $scope.unmatchedPaymentTotal = response.DataList.recordsTotal;
                }else{
                    $scope.unmatchedPayment = [];
                }
                if($scope.bookingReportParms.pageStart){
                    delete $scope.bookingReportParms.pageStart;
                }
            }).error(function(err,status) {
                $scope.errorView(err,status);
            });
        }    
    }

    $scope.totalAvailableCommsion = function(obj){

        var total = {};
        total.Bookings = 0;
        total.roomNights = 0;
        total.ExpectedCommission = 0;
        total.ActualCommission = 0;
        total.Pmt = 0;
        total.CXL = 0;
        total.DUE = 0;
        total.NC = 0;
        total.PP = 0;
        total.NS = 0;
        total.Other = 0;
        total.UnAns = 0;
        total.UnColl = 0; 

        for(var i = 0; i<obj.length;i++){
            if(obj[i].Bookings){
                total.Bookings =  Number(total.Bookings) + Number(obj[i].Bookings); 
            }
            if(obj[i].roomNights){
                total.roomNights =  Number(total.roomNights) + Number(obj[i].roomNights); 
            }
            if(obj[i].ExpectedCommission){
                total.ExpectedCommission =  Number(total.ExpectedCommission) + Number(obj[i].ExpectedCommission); 
            }

            if(obj[i].ActualCommission){
                total.ActualCommission =  Number(total.ActualCommission) + Number(obj[i].ActualCommission); 
            }
            if(obj[i].Pmt){
                total.Pmt =  Number(total.Pmt) + Number(obj[i].Pmt); 
            }
            if(obj[i].CXL){
                total.CXL =  Number(total.CXL) + Number(obj[i].CXL); 
            }
            if(obj[i].DUE){
                total.DUE =  Number(total.DUE) + Number(obj[i].DUE); 
            }
            if(obj[i].NC){
                total.NC =  Number(total.NC) + Number(obj[i].NC); 
            }
            if(obj[i].PP){
                total.PP =  Number(total.PP) + Number(obj[i].PP); 
            }
            if(obj[i].NS){
                total.NS =  Number(total.NS) + Number(obj[i].NS); 
            }
            if(obj[i].Other){
                total.Other =  Number(total.Other) + Number(obj[i].Other); 
            }
            if(obj[i].UnAns){
                total.UnAns =  Number(total.UnAns) + Number(obj[i].UnAns); 
            }
            if(obj[i].UnColl){
                total.UnColl =  Number(total.UnColl) + Number(obj[i].UnColl); 
            }
        }
        return total;
    }

    if(localStorage.getItem('filterDetailsFlag')){
        localStorage.removeItem('filterDetails')
        localStorage.removeItem('filterDetailsFlag')
    }else{
        localStorage.setItem('filterDetailsFlag', true);
    }

    if($stateParams.fromdate && $stateParams.todate){
        var fromDate = $stateParams.fromdate;
        var toDate = $stateParams.todate;
        var id = $stateParams.id;
    // $scope.unmatchedPaymentRpt.fromDate = new Date(fromDate);
    // $scope.unmatchedPaymentRpt.toDate = new Date(toDate);
    $scope.bookingReportParms.propertyChain = id;
    $scope.bookingReportParms.type = "1";


    if(localStorage.getItem('filterDetails')){
        $scope.filterDetails = JSON.parse(localStorage.getItem('filterDetails'));
        
        if($scope.filterDetails && $scope.filterDetails.type){
            $scope.bookingReportParms.type = $scope.filterDetails.type;
        }
        if($scope.filterDetails && $scope.filterDetails.unit){
            $scope.bookingReportParms.unit = $scope.filterDetails.unit;
        }
        if($scope.filterDetails && $scope.filterDetails.dateSelect){
            $scope.bookingReportParms.dateSelect = $scope.filterDetails.dateSelect;
        }
        if($scope.filterDetails && $scope.filterDetails.selectedDate){
            $scope.bookingReportParms.selectedDate = $scope.filterDetails.selectedDate;
        }
        if($scope.filterDetails && $scope.filterDetails.status){
            $scope.bookingReportParms.status = $scope.filterDetails.status;
        }
        if($scope.filterDetails && $scope.filterDetails.fromDate){
            var fDate = $scope.filterDetails.fromDate.split('/');
            $scope.filterDetails.fromDate = fDate[1] + "/" + fDate[0] + "/" + fDate[2];
            $scope.unmatchedPaymentRpt.fromDate = new Date($scope.filterDetails.fromDate);
            
        }
        if($scope.filterDetails && $scope.filterDetails.toDate){
            var tDate = $scope.filterDetails.toDate.split('/');
            $scope.filterDetails.toDate = tDate[1] + "/" + tDate[0] + "/" + tDate[2];
            $scope.unmatchedPaymentRpt.toDate = new Date($scope.filterDetails.toDate);
        }
        if($scope.filterDetails && $scope.filterDetails.branch){
            $scope.bookingReportParms.branch = $scope.filterDetails.branch;
        }
        if($scope.filterDetails && $scope.filterDetails.account){
            $scope.bookingReportParms.account = $scope.filterDetails.account;
        }
        if($scope.filterDetails && $scope.filterDetails.agent){
            $scope.bookingReportParms.agent = $scope.filterDetails.agent;
        }
        if($scope.filterDetails && $scope.filterDetails.iata){
            $scope.bookingReportParms.iata = $scope.filterDetails.iata;
        }
        if($scope.filterDetails && $scope.filterDetails.propertyBrand){
            $scope.bookingReportParms.propertyBrand = $scope.filterDetails.propertyBrand;
        }
        if($scope.filterDetails && $scope.filterDetails.propertyChain){
            $scope.bookingReportParms.propertyChain = $scope.filterDetails.propertyChain;
        }
        if($scope.filterDetails && $scope.filterDetails.propertyName){
            $scope.bookingReportParms.propertyName = $scope.filterDetails.propertyName;
        }
        if($scope.filterDetails && $scope.filterDetails.propertyAddress){
            $scope.bookingReportParms.propertyAddress = $scope.filterDetails.propertyAddress;
        }
        if($scope.filterDetails && $scope.filterDetails.propertyCity){
            $scope.bookingReportParms.propertyCity = $scope.filterDetails.propertyCity;
        }
        if($scope.filterDetails && $scope.filterDetails.propertyState){
            $scope.bookingReportParms.propertyState = $scope.filterDetails.propertyState;
        }
        if($scope.filterDetails && $scope.filterDetails.propertyZip){
            $scope.bookingReportParms.propertyZip = $scope.filterDetails.propertyZip;
        }
        if($scope.filterDetails && $scope.filterDetails.propertyPhone){
            $scope.bookingReportParms.propertyPhone = $scope.filterDetails.propertyPhone;
        }
        if($scope.filterDetails && $scope.filterDetails.propertyPreferred){
            $scope.bookingReportParms.propertyPreferred = $scope.filterDetails.propertyPreferred;
        }
        if($scope.filterDetails && $scope.filterDetails.accountGrp){
            $scope.bookingReportParms.accountGrp = $scope.filterDetails.accountGrp;
        }
        if($scope.filterDetails && $scope.filterDetails.rateCode){
            $scope.bookingReportParms.rateCode = $scope.filterDetails.rateCode;
        }
    }

    $scope.callFiterData(true);        
}


$scope.getPDFLink = function(expot){
    $scope.bookingReportParms.dataTableCommon.ExportType = Number(expot);
    if($scope.bookingReportParms.dataTableCommon.param.Columns){
        delete $scope.bookingReportParms.dataTableCommon.param.Columns;
    }
    $scope.callFiterData();
}

/* ==== Pagination Click function ==== */
$scope.logOrder = function (order) {
 $scope.bookingReportParms.dataTableCommon.param.Columns = [{}];
 if(order.charAt(0) == '-'){
    $scope.bookingReportParms.dataTableCommon.param.Columns[0].Name = order.slice(1);    
    $scope.bookingReportParms.dataTableCommon.param.Columns[0].Dir = "desc";    
}else{
    $scope.bookingReportParms.dataTableCommon.param.Columns[0].Name = order;    
    $scope.bookingReportParms.dataTableCommon.param.Columns[0].Dir = "asc";    
}
$scope.bookingReportParms.dataTableCommon.ExportType = 0;
$scope.callFiterData();

};

$scope.logPagination = function (page, limit) {
    var startPage = (limit * (page - 1));
    $scope.bookingReportParms.dataTableCommon.param.Start = startPage;
    $scope.bookingReportParms.dataTableCommon.param.Length = limit;
    $scope.bookingReportParms.dataTableCommon.ExportType = 0;
    $scope.callFiterData();
}



}]);