App.controller('userCreatedReportMaintenanceCtrl', ['$scope','authService','$cookies','$cookieStore','$state','$stateParams', '$http', 'GLOBALS','SETTINGS', 'toaster','$mdDialog', function($scope, authService,$cookies, $cookieStore, $state, $stateParams, $http, GLOBALS,SETTINGS,toaster, $mdDialog){
	
	var t;
	$scope.reportData = {
		report: "",
	}, t = angular.copy($scope.reportData), $scope.revert = function() {
		$scope.reportData = angular.copy(t), $scope.report_form.$setPristine(), $scope.report_form.$setUntouched()
	}, $scope.canRevert = function() {
		return !angular.equals($scope.reportData, t) || !$scope.report_form.$pristine
	}, $scope.canSubmit = function() {
		return $scope.report_form.$valid && !angular.equals($scope.reportData, t)
	}, $scope.submitForm = function() {
		$scope.showInfoOnSubmit = !0, e.revert()
	}

	
	$scope.userDetail = {};
	$scope.reportDetailsParams = {};
  $scope.updateReportDataParams = {};
  $scope.deleteReportParam = {};
  $scope.unitsObj = {};
  $scope.reportObj = {};
  $scope.loading = false;
  $scope.isReportData = false;
  $scope.reportListLoding = false;
  $scope.reportUpdateLoding = false;
  $scope.deleteReportLoading = false;


  /***** Get Report list *****/
  $scope.getReportList = function(){
    $scope.reportListLoding = true;
    $scope.reportDetailsParams.userID = $scope.userDetails.userID;

    $http({
      url: SETTINGS[GLOBALS.ENV].apiUrl+"Report/GetCustomReportList",
      method: 'POST',
      data: $.param($scope.reportDetailsParams),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
      }
    }).success(function(response) { 
     $scope.reportListLoding = false;
     if(response.ResposeCode == 2){
      if(response.DataList.data){
        $scope.reportList = response.DataList.data;
      }    
    }else{

    }
  });
  }

  $scope.getReportList();
  /***** Get Report list *****/




  /***** Get Report Data by report_id *****/
  $scope.getReportData = function(report){

    $scope.loading = true;
    $scope.id = report.report;

    $scope.userDetail.corpID = $scope.userDetails.corpID;
    $scope.userDetail.userID = $scope.userDetails.userID;
    $scope.userDetail.report_id = report.report;
    $scope.userDetail.parameters = "";

        /*$scope.userDetail.corpID = 111;
        $scope.userDetail.userID = "GARBER";
        $scope.userDetail.report_id = 60;
        $scope.userDetail.parameters = "";*/
        
        $http({
          url: SETTINGS[GLOBALS.ENV].apiUrl+"Report/GetReportData",
          method: 'POST',
          data: $.param($scope.userDetail),
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
          }
        }).success(function(response) { 
        	$scope.loading = false;

          if(response.ResposeCode == 2){
            if(response.DataList.data){
              $scope.isReportData = true;
              $scope.reportData = response.DataList.data;

              if(response.DataList.data && response.DataList.data != ''){
                if(response.DataList.data[0].userData.userID){
                  $scope.reportData.createdByUser = response.DataList.data[0].userData.userID + " - " + response.DataList.data[0].userData.name; 
                }
              }    
            }
          }else{

          }
        });
      };
      /***** Get Report Data by report_id *****/




      /***** Update Report Data *****/
      $scope.updateReport = function(userMaintenanceDetails){

       $scope.updateReportDataParams.id = $scope.id;
       $scope.updateReportDataParams.title = userMaintenanceDetails[0].WebReportData.title;
       $scope.updateReportDataParams.description = userMaintenanceDetails[0].WebReportData.description;
       $scope.updateReportDataParams.parameters = userMaintenanceDetails[0].WebReportData.parameters;
       $scope.updateReportDataParams.active = userMaintenanceDetails[0].WebReportData.active;

       $scope.reportUpdateLoding = true;

       /*$scope.updateReportDataParams.id = $scope.id;
       $scope.updateReportDataParams.title = userMaintenanceDetails[0].WebReportData.title;
       $scope.updateReportDataParams.description = userMaintenanceDetails[0].WebReportData.description;
       $scope.updateReportDataParams.parameters = "";
       $scope.updateReportDataParams.active = userMaintenanceDetails[0].WebReportData.active;        */

       $http({
        url: SETTINGS[GLOBALS.ENV].apiUrl+"Report/UpdateCustomReportData",
        method: 'POST',
        data: $.param($scope.updateReportDataParams),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': 'bearer '+authService.getAuthToken('AuthToken')                

        }
      }).success(function(response) { 
       $scope.reportUpdateLoding = false;
       if(response.ResposeCode == 2){
        toaster.pop('Success', '', response.ResponseMessage);
      }else{
        toaster.pop('error', '', response.ResponseMessage);
      }
    });

    }
    /***** Update Report Data *****/



    /***** Delete Report Data *****/
    $scope.deleteReport = function(){
      $scope.deleteReportLoading = true;
      $scope.deleteReportParam.id = $scope.id;

      $http({
        url: SETTINGS[GLOBALS.ENV].apiUrl+"Report/DeleteCustomReportData",
        method: 'POST',
        data: $.param($scope.deleteReportParam),
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': 'bearer '+authService.getAuthToken('AuthToken')                

        }
      }).success(function(response) { 
        $scope.deleteReportLoading = false;
        if(response.ResposeCode == 2){
          toaster.pop('Success', '', response.ResponseMessage);
          $scope.getReportList();
          $scope.cancelUpdate();
        }else{
          toaster.pop('error', '', response.ResponseMessage);
        }
      });
    }
    /***** Delete Report Data *****/



    /***** Delete popup *****/
    $scope.popupTitle = 'Delete Report';
    $scope.pageDescription = 'Would you like to delete?';

    $scope.deleteReportConfirm = function(ev){
     $mdDialog.show({
      contentElement: '#conFirmPopup',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      fullscreen: false
    }).finally(function() {
    });
  }

  $scope.callBackDialog = function(data){
    if(data == 'ok'){
      /*Call delete API here */
      $scope.deleteReport();
    }
    $mdDialog.cancel();
  }

  $scope.cancelUpdate = function(){
    $scope.isReportData = false;
    $scope.report = "";
  }

}]);