App.controller('chainCodeSummaryCtrl', ['$scope','$state','$stateParams', '$rootScope', 'MetaInformation','$http', 'SETTINGS', 'GLOBALS', 'authService','toaster','$mdDialog','$filter', function($scope,$state,$stateParams, $rootScope, MetaInformation,$http, SETTINGS, GLOBALS, authService,toaster,$mdDialog,$filter) {

    $scope.commissionByAcountObj = {};
    $scope.commissionByAcountObj.fromDate = null;
    $scope.commissionByAcountObj.toDate = null;
    $scope.commissionByAcountObj.selectedDate = 0;
    $scope.setselectedDate = 0;

    $scope.noData = "No data found";
    $scope.corpID = $scope.userDetails.corpID;
    $scope.chainCodeListLoading = false;
    var date = new Date();
    var toDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    var currentDate = new Date();
    currentDate.setMonth((toDate.getMonth() - 11));

    if((toDate.getMonth() + 1) == 12){
        $scope.frmDate = 1 + "/" + (currentDate.getMonth()+1) + "/" + (toDate.getFullYear());
    }else{
        $scope.frmDate = 1 + "/" + (currentDate.getMonth()+1) + "/" + (toDate.getFullYear()-1);    
    }
    $scope.toDate = (toDate.getDate()) + "/" + (toDate.getMonth() + 1) + "/" + toDate.getFullYear();
    
    if((toDate.getMonth() + 1) == 12){
        $scope.frmDateID = (currentDate.getMonth()+1) + "/" + 1 + "/" + (toDate.getFullYear());
    }else{
        $scope.frmDateID = (currentDate.getMonth()+1) + "/" + 1 + "/" + (toDate.getFullYear()- 1);
    }
    $scope.toDateID = (toDate.getMonth() + 1) + "/" + (toDate.getDate()) + "/" + toDate.getFullYear();

    $scope.bookingReportParms = {
        dataTableCommon:{
            param:{
                Draw:1,
                Start:0,
                Length:10
            },
            ExportType:0
        },
        // corpid:$scope.corpID,
        
        /*fromdate:$scope.frmDate,
        todate:$scope.toDate,*/
        corpID:$scope.corpID,
        fromDate:$scope.frmDate,
        toDate:$scope.toDate,
        dateSelect:0,
        type:"1"
    };

    /* ===== CallBack Global Left panel View ===== */
    $scope.copyObj = angular.copy($scope.bookingReportParms);
    $scope.$emit('callSelectedFieldByPage',$scope.bookingReportParms);

    $scope.$on('callSelectedField', function(evnt,obj) { 
        var finalObj = angular.copy($scope.copyObj);
        // $scope.bookingReportParms = Object.assign(finalObj,obj); 
        $scope.bookingReportParms = $.extend(finalObj, obj);
        $scope.callFiterData(true);
    });

    function createFilterFor(query) {
        var teamIsNew = $scope.indexedTeamsA.indexOf(query.iata) == -1;
        if (teamIsNew) {
            $scope.indexedTeamsA.push(query.iata);
        }
        return teamIsNew;
    }

    function createFilterForChild(query) {
        var teamIsNew = $scope.indexedTeamsB.indexOf(query.cts_type) == -1;
        if (teamIsNew) {
            $scope.indexedTeamsB.push(query.cts_type);
        }
        return teamIsNew;
    }

    function filterDate(obj){
        if(obj.deposit_date){
            obj.deposit_date = new Date(obj.deposit_date);
        }
        if(obj.in_date){
            obj.in_date = new Date(obj.in_date);
        }
        if(obj.out_date){
            obj.out_date = new Date(obj.out_date);
        }
        if(obj.trans_date){
            obj.trans_date = new Date(obj.trans_date);
        }
        if(obj.inv_date){
            obj.inv_date = new Date(obj.inv_date);
        }
        return obj;
    }

    $scope.checkValidDate = function(fromDate,toDate){
        if(toDate && fromDate > toDate){
            $scope.commissionByAcountObj.toDate = fromDate;
        }
    }

    $scope.callFiterData = function(valid){
        if(valid){
            $scope.bookingReportParms.dataTableCommon.ExportType = 0;
        }
        if(!$scope.bookingReportParms.dataTableCommon.ExportType){
            $scope.chainCodeListLoading = true;
            $scope.$emit('callSelectedFieldByPage',$scope.bookingReportParms);
            if($scope.bookingReportParms.fromDate){
                $scope.frmDateID = $scope.bookingReportParms.fromDate
            }
            if($scope.bookingReportParms.toDate){
                $scope.toDateID = $scope.bookingReportParms.toDate
            }
            if($scope.bookingReportParms.pageStart){
                $scope.query.page = 1;
            }
            if(localStorage.getItem('filterDetailsFlag')){
                localStorage.removeItem('filterDetailsFlag');
            }
            localStorage.setItem('filterDetails', JSON.stringify($scope.bookingReportParms));
        }
        $http({
            url: SETTINGS[GLOBALS.ENV].apiUrl+"GetBookingsResolution/GetBookingsResolution_ByChainCode",
            method: 'POST',
            data: $scope.bookingReportParms,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
            }
        }).success(function(response) {
            $scope.chainCodeListLoading = false;
            
            if(response.ResposeCode == 2){
                if(response.DataList.data){
                    $scope.chainCodeList = response.DataList.data;
                }
                if(response.DataList.fileName){
                    /*window.open(
                        SETTINGS[GLOBALS.ENV].apiUrl+response.DataList.fileName,
                        '_blank'
                        );*/
                        window.open(
                            response.DataList.fileName,
                            '_blank'
                            );
                    }
                    $scope.bookingListTotal = response.DataList.recordsTotal;
                }else{
                    $scope.chainCodeList = [];
                }
                if($scope.bookingReportParms.pageStart){
                    delete $scope.bookingReportParms.pageStart;
                }
            }).error(function(err,status) {
                $scope.errorView(err,status);
            });
        }

        $scope.callFiterData(true);

        $scope.redirectPage = function(item){
        // $state.go("main.commissionAnalysisById", {fromDate:$scope.bookingReportParms.fromDate, toDate:$scope.bookingReportParms.toDate, id:item.chain, account: $scope.bookingReportParms.account, accountGrp: $scope.bookingReportParms.accountGrp, agent: $scope.bookingReportParms.agent, branch: $scope.bookingReportParms.branch, corpID: $scope.bookingReportParms.corpID, dateSelect: $scope.bookingReportParms.dateSelect, iata: $scope.bookingReportParms.iata, propertyAddress: $scope.bookingReportParms.propertyAddress, propertyBrand: $scope.bookingReportParms.propertyBrand, propertyChain: $scope.bookingReportParms.propertyChain, propertyCity: $scope.bookingReportParms.propertyCity, propertyName: $scope.bookingReportParms.propertyName, propertyPhone: $scope.bookingReportParms.propertyPhone, propertyPreferred: $scope.bookingReportParms.propertyPreferred, propertyState: $scope.bookingReportParms.propertyState, propertyZip: $scope.bookingReportParms.propertyZip, rateCode: $scope.bookingReportParms.rateCode, selectedDate: $scope.bookingReportParms.selectedDate, status: $scope.bookingReportParms.status, type: $scope.bookingReportParms.type, unit: $scope.bookingReportParms.unit});
    }

    $scope.getPDFLink = function(expot){
        $scope.bookingReportParms.dataTableCommon.ExportType = Number(expot);
        $scope.callFiterData();
    }

    /* ==== Pagination Click function ==== */
    $scope.logOrder = function (order) {
        
    };

    $scope.logPagination = function (page, limit) {
        var startPage = (limit * (page - 1));
        $scope.bookingReportParms.dataTableCommon.param.Start = startPage;
        $scope.bookingReportParms.dataTableCommon.param.Length = limit;
        $scope.bookingReportParms.dataTableCommon.ExportType = 0;
        $scope.callFiterData();
    }

}]);
