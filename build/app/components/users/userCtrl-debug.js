App.controller('userCtrl', ['$scope', '$rootScope', 'MetaInformation','$http', 'SETTINGS', 'GLOBALS', 'authService', 'toaster',  '$state', function($scope, $rootScope, MetaInformation,$http, SETTINGS, GLOBALS, authService, toaster, $state) {
    // $scope.name = 'Home page';
    
    $scope.seo = {
      "seo_title": "Home page",
      "seo_description": "Home page description",
      "seo_keywords": "Home page Keywords"
    };
    setMetaData($scope, $rootScope, MetaInformation, $scope.seo);
    // $scope.variable = 'Variable Name';

    var t;
    $scope.userData = {
      userID: "",
    }, t = angular.copy($scope.userData), $scope.revert = function() {
      $scope.userData = angular.copy(t), $scope.login_form.$setPristine(), $scope.login_form.$setUntouched()
    }, $scope.canRevert = function() {
      return !angular.equals($scope.userData, t) || !$scope.login_form.$pristine
    }, $scope.canSubmit = function() {
      return $scope.login_form.$valid && !angular.equals($scope.userData, t)
    }, $scope.submitForm = function() {
      $scope.showInfoOnSubmit = !0, e.revert()
    }

    $scope.userDetail = {};
    $scope.unitsObj = {};
    $scope.updateLoading = false;
    $scope.userUpdateLoding = false;
	// $scope.userId = "GARBER";

  /***** Get the User list *****/
  $scope.changePassword = function(userDetails){
    delete $scope.changePasswordMessage;
    
    if(!userDetails){
      toaster.pop('error', '', 'Please Enter Current Password.'); 
      return;
    }
    $scope.userDetail.userId = $scope.userDetails.userID;
    $scope.userDetail.NewPassword = userDetails.password;
    $scope.userDetail.CurrentPassword = userDetails.currentPassword;
    $scope.userDetail.ConfirmPassword = userDetails.confirmPassword;
    
    if(!$scope.userDetail.CurrentPassword){
      toaster.pop('error', '', 'Please Enter Current Password.'); 
      return;
    }

    if(!$scope.userDetail.NewPassword){
      toaster.pop('error', '', 'Please Enter New Password.'); 
      return;
    }

    if(!$scope.userDetail.ConfirmPassword){
      toaster.pop('error', '', 'Please Enter ReEnter Password.'); 
      return;
    }

    if($scope.userDetail.NewPassword != $scope.userDetail.ConfirmPassword){
      toaster.pop('error', '', 'Password and ReEnter password does not match.'); 
      return;
    }

    $scope.updateLoading = true;

    $http({
      url: SETTINGS[GLOBALS.ENV].apiUrl+"ChangePassword",
      method: 'POST',
      data: $.param($scope.userDetail),
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
      }
    }).success(function(response) { 
     $scope.updateLoading = false;
     if(response.ResposeCode == 2){
      toaster.pop('Success', '', response.ResponseMessage);	
      $scope.signout();

    }else{
      // toaster.pop('error', '', response.ResponseMessage);	
      $scope.changePasswordMessage = response.ResponseMessage
    }
    
  });
  }

  $scope.redirectToHomePage = function(){
    $state.go("main.home");
  }

  $scope.submitBtn = true;

  $scope.checkPassword = function(password, confirmPassword){
   if(password && confirmPassword)
    if(password != confirmPassword){
     $scope.submitBtn = true;
     return "Password and ReEnter password does not match.";
   }else{
    if(!$scope.user.currentPassword){
      $scope.submitBtn = true;
    }else{
      $scope.submitBtn = false;
    }
  }
}

$scope.checkPasswordForAddUser = function(password, confirmPassword){
  if(password && confirmPassword){

    if(password != confirmPassword){
     $scope.login_form.$valid = false;
     return "Password and ReEnter password does not match.";
   }else{
      // $scope.login_form.$valid = true;
      $scope.login_form.reEnterPassword.$error = true;
      $scope.login_form.reEnterPassword.$dirty = true;
    }
  }
}


/***** Restrict user by -> Unit*****/

$scope.getUnits = function(){

  $scope.unitsObj.id = $scope.userDetails.corpID;
  $scope.unitsObj.unit = "";

  $http({
    url: SETTINGS[GLOBALS.ENV].apiUrl+"Corp_Agency/GetCorpBusinessUnits",
    method: 'POST',
    data: $.param($scope.unitsObj),
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
    }
  }).success(function(response) { 

    if(response.ResposeCode == 2){
      $scope.unitList = response.DataList.data;
    }else{

    }
  });
}

$scope.getUnits();
/***** Restrict user by -> Unit*****/

var uId = {};
uId.userID = ($scope.userDetails)?$scope.userDetails.user.id:''
$scope.getReportList = function(){
  $http({
   url: SETTINGS[GLOBALS.ENV].apiUrl+"Report/GetReportList",
   method: 'POST',
   data: {userID:""},
   // data: uId,
   headers: {
     'Content-Type': 'application/json',
     'Authorization': 'bearer '+authService.getAuthToken('AuthToken')                

   }
 }).success(function(response) { 
   if(response.ResposeCode == 2){
     $scope.userData.userReports = response.DataList.data;
   }else{

   }
 });
}        
$scope.getReportList();

function filterBoolean(obj,key){
  angular.forEach(obj, function(value, key) {
   if(typeof obj[key] == 'boolean'){
    if(obj[key]){
     obj[key] = 1;
   }else{
     obj[key] = 0;
   }
 }
});
  return obj;
}

function getOnlySelectedReports(obj){
  if(obj.selected == true){
    return obj;
  }
}

/*$scope.enableOrdisableCalenderFields = function(value){
  console.log("value : ", value);

  if(!value){
    $scope.dateCheck = false;
  }
}*/


/***** Add User Data *****/
$scope.insertUserData = function(userMaintenanceDetails){
  userMaintenanceDetails.user.ta_id = $scope.userDetails.ta_id;
  userMaintenanceDetails.user.passwordNew = userMaintenanceDetails.user.passwordNew;
  var useData = [userMaintenanceDetails.user];
  
  if(userMaintenanceDetails.user_Permissions && userMaintenanceDetails.user_Permissions.businessCalendar){
    if(userMaintenanceDetails.user_Permissions.businessCalendar == false){
      userMaintenanceDetails.user_Permissions.maintainBusinessCalendar_Dates = false;
      userMaintenanceDetails.user_Permissions.maintainBusinessCalendar_Close = false;
    }
  }
  if(userMaintenanceDetails.user_Permissions && userMaintenanceDetails.user_Permissions.paymentRecordupdate){
    if(userMaintenanceDetails.user_Permissions.paymentRecordupdate == false){
      userMaintenanceDetails.user_Permissions.updatePayments_Type = false;
      userMaintenanceDetails.user_Permissions.updatePayments_PostRelease_Confirmation = false; 
      userMaintenanceDetails.user_Permissions.updatePayments_PropertyChain = false; 
      userMaintenanceDetails.user_Permissions.updatePayments_DepositDate = false;
      userMaintenanceDetails.user_Permissions.updatePayments_LastName = false;
      userMaintenanceDetails.user_Permissions.updatePayments_PostRelease_IATA = false;
      userMaintenanceDetails.user_Permissions.updatePayments_PropertyName = false; 
      userMaintenanceDetails.user_Permissions.updatePayments_CheckNo = false;
      userMaintenanceDetails.user_Permissions.updatePayments_FirstName = false;
      userMaintenanceDetails.user_Permissions.updatePayments_PostRelease_Branch = false;
      userMaintenanceDetails.user_Permissions.updatePayments_PropertyAddress = false;
      userMaintenanceDetails.user_Permissions.updatePayments_CheckDate = false;
      userMaintenanceDetails.user_Permissions.updatePayments_InDate = false;
      userMaintenanceDetails.user_Permissions.updatePayments_PostRelease_Agent = false;
      userMaintenanceDetails.user_Permissions.updatePayments_PropertyCity = false;
      userMaintenanceDetails.user_Permissions.updatePayments_Commissions = false;
      userMaintenanceDetails.user_Permissions.updatePayments_OutDate = false;
      userMaintenanceDetails.user_Permissions.updatePayments_PostRelease_Account = false;
      userMaintenanceDetails.user_Permissions.updatePayments_PropertyState = false;
      userMaintenanceDetails.user_Permissions.updatePayments_Tax1 = false;
      userMaintenanceDetails.user_Permissions.updatePayments_Nights = false;
      userMaintenanceDetails.user_Permissions.updatePayments_PropertyZip = false;
      userMaintenanceDetails.user_Permissions.updatePayments_Rooms = false;
      userMaintenanceDetails.user_Permissions.updatePayments_PropertyCountry = false;
      userMaintenanceDetails.user_Permissions.updatePayments_Rate = false;
      userMaintenanceDetails.user_Permissions.updatePayments_PropertyPhone = false;
      userMaintenanceDetails.user_Permissions.updatePayments_RateCode = false;
      userMaintenanceDetails.user_Permissions.updatePayments_PropertyFax = false;
      userMaintenanceDetails.user_Permissions.updatePayments_CorporateDiscount = false;
      userMaintenanceDetails.user_Permissions.updatePayments_HotelID = false;
      userMaintenanceDetails.user_Permissions.updatePayments_BookingPCC = false;
      userMaintenanceDetails.user_Permissions.updatePayments_AgentPCC = false;
    }
  }
  
  if(userMaintenanceDetails.user_Permissions){
    var userPermissionsData = [userMaintenanceDetails.user_Permissions];  
  }
  

    // $scope.user = {};
    var userObj = useData.filter(filterBoolean);
		// userObj.user_id = $scope.user.user_id;
		// var userPermissionsObj = userPermissionsData.filter(filterBoolean);
		// userObj.userID = $scope.user_id;
		// userObj.ta_id = $scope.userDetails.ta_id;

		var reportList;
		reportList = userMaintenanceDetails.userReports.filter(getOnlySelectedReports);

		var finalObj = {
			user:userObj[0],
			user_Permissions:(userPermissionsData)?userPermissionsData[0]:{},
      user_Reports: reportList
    }

    if(finalObj.user_Permissions && finalObj.user_Permissions.businessCalendar == false){
      finalObj.user_Permissions.maintainBusinessCalendar_Dates = false;
      finalObj.user_Permissions.maintainBusinessCalendar_Close = false;
    }

    if(!finalObj.user.userID){
      toaster.pop('error', '', "Please Enter User Id");
      return;
    }

    if(!finalObj.user.name){
      toaster.pop('error', '', "Please Enter Name");
      return;
    }

    if(!finalObj.user.email){
      toaster.pop('error', '', "Please Enter Email");
      return;
    }

    if(!finalObj.user.passwordNew){
      toaster.pop('error', '', "Please Enter Password");
      return;
    }

    if(!finalObj.user.reEnterPassword){
      toaster.pop('error', '', "Please Enter ReEnter Password.");
      return;
    }

    if(finalObj.user.passwordNew && finalObj.user.reEnterPassword){
      if(finalObj.user.passwordNew != finalObj.user.reEnterPassword){
        toaster.pop('error', '', "Password and ReEnter password does not match.");
        return;   
      }
    }

    $scope.userUpdateLoding = true;
    // if(!finalObj.user.name){
    //   toaster.pop('error', '', "Please Enter Name");
    //   return;
    // }

    $http({
      url: SETTINGS[GLOBALS.ENV].apiUrl+"User/InsertUserData",
      method: 'POST',
      data: finalObj,
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+authService.getAuthToken('AuthToken')                

      }
    }).success(function(response) { 
     $scope.userUpdateLoding = false;
     if(response.ResposeCode == 2){
        		// $scope.getUserData();
        		toaster.pop('Success', '', response.ResponseMessage);
        		$state.go("main.userMaintenance");

        	}else{		
            toaster.pop('error', '', response.ResponseMessage);
          }
        });

  }

  $scope.reDirectPage = function(){
    $state.go("main.userMaintenance");
  }

}]);