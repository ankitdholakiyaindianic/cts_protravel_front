App.controller('accountMaintenance', ['$scope','authService','$cookies','$cookieStore','$state','$stateParams', '$http', 'GLOBALS','SETTINGS', 'toaster','$mdDialog','$rootScope', '$timeout', function($scope, authService,$cookies, $cookieStore, $state, $stateParams, $http, GLOBALS,SETTINGS,toaster, $mdDialog,$rootScope, $timeout){
	
	var t;
    $scope.stateName = '';
    $scope.accountDetails = {};
    $scope.accountDetails.id = 0;
    $scope.accountDetails.corpID = $scope.userDetails.corpID;
    $scope.accountDetails.reassignedAgentID = "";
    $scope.accountDetails.reassignedAccountID = "";
    $scope.accountDetails.accountType = "";
    $scope.accountDetails.group = "";
    $scope.accountDetails.active = true;
    $scope.accountListLoading = false;
    $scope.noData = "No data found";
    $scope.corpID = $scope.userDetails.corpID;
    $scope.disableAccountNo = false;

    t = angular.copy($scope.accountDetails), $scope.revert = function() {
        $scope.accountDetails = angular.copy(t), $scope.accountMaintenanceForm.$setPristine(), $scope.accountMaintenanceForm.$setUntouched()
    }, $scope.canRevert = function() {
        return !angular.equals($scope.accountDetails, t) || !$scope.accountMaintenanceForm.$pristine
    }, $scope.canSubmit = function() {
        return $scope.accountMaintenanceForm.$valid && !angular.equals($scope.accountDetails, t)
    }, $scope.submitForm = function() {
        $scope.showInfoOnSubmit = !0, e.revert()
    }

    // $scope.limitOptions = [10,25,50];
    $scope.limitOptions = [50, 100];
    $scope.query = {};
    // $scope.query.limit = 10;
    $scope.query.limit = 50;
    $scope.query.page = 1;
    $scope.getAccountParam = {
        DataTableCommon:{
            param:{
                Draw:1,
                Start:0,
                // Length:10
                Length:50
            },
            ExportType:0
        },
        corpID:$scope.corpID,group:"",description:"",accountCode:"",active:""
    };

    $scope.getAccountMaintenanceList = function(valid){
        // if(valid){
        //     $scope.setRequestList.DataTableCommon.param.Start = 0;
        //     $scope.setRequestList.DataTableCommon.param.Length = 10;
        //     if($scope.setRequestList.DataTableCommon.param.Columns){
        //         delete $scope.setRequestList.DataTableCommon.param.Columns;
        //     }
        // }
        if(valid){
            $scope.accountListLoading = true;
            $scope.setRequestList.DataTableCommon.ExportType = 0;
        }
        $scope.setRequestList.searchText = "";
        $http({
            url: SETTINGS[GLOBALS.ENV].apiUrl+"Corp_Account/GetCorpAccountList",
            method: 'POST',
            data: $scope.setRequestList,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'bearer '+authService.getAuthToken('AuthToken'),
            }
        }).success(function(response) { 
            $scope.accountListLoading = false;
            if(response.ResposeCode == 2){
                if(response.DataList.data){
                    $scope.accountList = response.DataList.data;
                    $scope.accountListTotal = response.DataList.recordsTotal;
                    /*===== For Fixed header =====*/
                    $timeout(function () {

                        if ($("md-table-container .md-table:first").length > 0) {
                            $("md-table-container .md-table:first").floatThead('destroy');
                            $("md-table-container .md-table:first").floatThead({ scrollingTop: 60 });
                        }

                    }, 2);
                }
                if(response.DataList.fileName){
                    /*window.open(
                        SETTINGS[GLOBALS.ENV].apiUrl+response.DataList.fileName,
                        '_blank'
                        );*/
                        window.open(
                            response.DataList.fileName,
                            '_blank'
                            );
                    }
                // $scope.accountListTotal = response.DataList.recordsTotal;
            }
        }).error(function(err,status) {
            /*===== For Fixed header =====*/
            var $table = $('.md-table:first');
            $table.floatThead({
              responsiveContainer: function($table){
                  return $table.closest('.full-height');
              }

          });
            $scope.errorView(err,status);
        });
    }

    if($state.current.name == 'main.accountMaintenance'){
        $scope.stateName = 'list';
        $scope.accountDetails = {};
        $scope.accountDetails.corpID = $scope.corpID;
        $scope.accountDetails.active = "";
        $scope.accountListLoading = true;
        $scope.setRequestList = angular.copy($scope.getAccountParam);
        $scope.getAccountMaintenanceList(true);

    }else{
        if($stateParams.id){
            $scope.stateName = 'update';
            $scope.accountID = $stateParams.id;
            $scope.obj = {};
            $scope.obj.id = $stateParams.id;
            $scope.obj.accountCode = "";
            $scope.obj.corpID = "";
            $scope.disableAccountNo = true;

            $http({
                url: SETTINGS[GLOBALS.ENV].apiUrl+"Corp_Account/GetAccountData",
                method: 'POST',
                data: $.param($scope.obj),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
                }
            }).success(function(response) { 
                if(response.DataModel){
                    $scope.accountDetails = response.DataModel.CorpAccount;
                }
                
            }).error(function(err,status) {
                $scope.errorView(err,status);
            });
        }else{
            if($state.current.name == 'main.accountMaintenanceAdd'){
                $scope.stateName = 'add';
            }
        }
    }

    $scope.callAddAccount = function(){
        $state.go('main.accountMaintenanceAdd');
    }

    $scope.reDirectPage = function(){
        $state.go('main.accountMaintenance')    ;   
    }
    

    /***** Add and update account *****/
    $scope.addAndUpdate = function(accountDetails){

        if(!accountDetails.accountCode){
            toaster.pop('error', '', 'Please Enter Account Code.');
            return;
        }

        if(!accountDetails.description){
            toaster.pop('error', '', 'Please Enter Description.');
            return;
        }

        $scope.userListLoding = true;
        var API_Name = 'Corp_Account/InsertAccount';
        if(accountDetails.id){
            API_Name = 'Corp_Account/UpdateAccount';
        }else{
            accountDetails['id'] = 0;
        }
        accountDetails['corpID'] = $scope.corpID;

        $http({
            url: SETTINGS[GLOBALS.ENV].apiUrl+API_Name,
            method: 'POST',
            data: $.param($scope.accountDetails),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
            }
        }).success(function(response) { 
            if(response.ResposeCode == 2){
                toaster.pop('Success', '', response.ResponseMessage);
                $scope.reDirectPage();
            }else{
                toaster.pop('error', '', response.ResponseMessage);
            }
        });
    }

    
    /***** Search Data *****/
    $scope.searchData = function(search){
        if(!search){
            search = "";    
        }else{
            search = angular.lowercase(search);    
            search = search.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
        }
        
        $scope.setRequestList.searchText = search;
        $scope.accountList = [];
        $scope.accountListLoading = true;

        $http({
          url: SETTINGS[GLOBALS.ENV].apiUrl+"Corp_Account/GetCorpAccountList",
          method: 'POST',
          data: $scope.setRequestList,
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer '+authService.getAuthToken('AuthToken')                

        }
    }).success(function(response) { 
        $scope.accountListLoading = false;
        if(response.ResposeCode == 2){
            $scope.accountList = response.DataList.data;
            $scope.accountListTotal = response.DataList.recordsTotal;
        }else{      
            $scope.accountList = "";
            /*toaster.pop('error', '', response.ResponseMessage);*/
        }
    });
}

$scope.deleteAccount = function(){

    $scope.id = $stateParams.id;
    var obj = {};
    obj.id = $scope.id;
        //return false;
        $http({
            url: SETTINGS[GLOBALS.ENV].apiUrl+"Corp_Account/DeleteAccount",
            method: 'POST',
            data: $.param(obj),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
            }
        }).success(function(response) { 
            if(response.ResposeCode == 2){
                toaster.pop('Success', 'Delete', response.ResponseMessage);
                $scope.reDirectPage();
            }else{
                toaster.pop('error', 'Delete', response.ResponseMessage);
            }
        });
    }
    /***** Add and update account *****/

    /* ==== get PDF Link ==== */
    $scope.getPDFLink = function(expot){
        $scope.setRequestList.DataTableCommon.ExportType = Number(expot);
        if($scope.setRequestList.DataTableCommon.param.Columns){
            delete $scope.setRequestList.DataTableCommon.param.Columns;
        }
        $scope.getAccountMaintenanceList();
    }

    /* ==== Pagination Click function ==== */
    $scope.logOrder = function (order) {
        $scope.setRequestList.DataTableCommon.param.Columns = [{}];
        if(order.charAt(0) == '-'){
            $scope.setRequestList.DataTableCommon.param.Columns[0].Name = order.slice(1);    
            $scope.setRequestList.DataTableCommon.param.Columns[0].Dir = "desc";    
        }else{
            $scope.setRequestList.DataTableCommon.param.Columns[0].Name = order;    
            $scope.setRequestList.DataTableCommon.param.Columns[0].Dir = "asc";    
        }

        $scope.storeColumns = $scope.setRequestList.DataTableCommon.param.Columns;
        $scope.setRequestList.DataTableCommon.ExportType = 0;
        $scope.getAccountMaintenanceList(true);

    };

    $scope.logPagination = function (page, limit) {
        var startPage = (limit * (page - 1));
        $scope.setRequestList = angular.copy($scope.getAccountParam);
        $scope.setRequestList.DataTableCommon.param.Columns = $scope.storeColumns;
        $scope.setRequestList.DataTableCommon.param.Start = startPage;
        $scope.setRequestList.DataTableCommon.param.Length = limit;
        $scope.setRequestList.DataTableCommon.ExportType = 0;
        $scope.getAccountMaintenanceList(true);
    }
    
    /***** Delete popup *****/
    $scope.popupTitle = 'Delete Account';
    $scope.pageDescription = 'Would you like to delete?';
    $scope.showConfirmPopup = function(ev,id) {
        $mdDialog.show({
            contentElement: '#conFirmPopup',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:true,
            fullscreen: false
        }).finally(function() {
        });
    };

    $scope.callBackDialog = function(data){
        if(data == 'ok'){
            /*Call delete API here */
            $scope.deleteAccount();
        }
        $mdDialog.cancel();
    }

    /***** Delete popup *****/

}]);