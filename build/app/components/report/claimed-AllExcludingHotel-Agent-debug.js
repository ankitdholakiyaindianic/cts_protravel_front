App.controller('claimed-AllExcludingHotel-Agent', ['$scope','$state','$stateParams', '$rootScope', 'MetaInformation','$http', 'SETTINGS', 'GLOBALS', 'authService','toaster','$mdDialog','$filter','$timeout', function($scope,$state,$stateParams, $rootScope, MetaInformation,$http, SETTINGS, GLOBALS, authService,toaster,$mdDialog,$filter,$timeout) {

    $scope.matureBookByAgentCarsOnly = {};
    $scope.matureBookByAgentCarsOnly.fromDate = null;
    $scope.matureBookByAgentCarsOnly.toDate = null;
    /* ==== pagination options ==== */
    // $scope.limitOptions = [10,25,50];
    $scope.limitOptions = [50, 100];
    $scope.query = {};
    // $scope.query.limit = 10;
    $scope.query.limit = 50;
    $scope.query.page = 1;

    $scope.noData = "No data found";
    $scope.corpID = $scope.userDetails.corpID;
    $scope.unmatchedPaymentLoading = false;
    $scope.clearDateDropDown = true;

    $scope.parseINT = function(val){
        return parseInt(val);
    }

    $scope.bookingReportParms = {
        dataTableCommon:{
            param:{
                Draw:1,
                Start:0,
                // Length:10
                Length:50
            },
            ExportType:0
        },
        corpID:$scope.corpID,
        fromDate:($scope.matureBookByAgentCarsOnly.fromDate) ? $scope.matureBookByAgentCarsOnly.fromDate : "",
        toDate:($scope.matureBookByAgentCarsOnly.toDate) ? $scope.matureBookByAgentCarsOnly.toDate : "",
        TAID:"",
        dateSelect:"",
        unit:"",
        branch:"",
        agent:($scope.userDetails.agent) ? $scope.userDetails.agent : "",
        iata:"",
        account:"",
        propertyBrand:"",
        propertyChain:"",
        propertyName:"",
        propertyAddress:"",
        propertyCity:"",
        propertyState:"",
        propertyZip:"",
        propertyPhone:"",
        propertyPreferred:"",
        lastName:"",
        firstName:"",
        // type:"0,2,3,4,5,6,7,8,9",
        type:"2",
        status:"",
        claimed:"",
        amount:"1",
        amountSign:"4",
        amountDollar:"0",
        checkNo:"",
        batchNo:"",
        recordSelect:"",
        bookingSource:"",
        accountGroup:"",
        pnr:"",
        rateCode:"",
        groupType:"1",
        claimed: true,
        confirmation: "",
        payment_id: null,
        inDate: ""
    };
    $scope.clearDateDropDownSelectedFlag = false;

    $scope.copyObj = angular.copy($scope.bookingReportParms);
    $scope.$emit('callSelectedFieldByPage',$scope.bookingReportParms);

    $scope.$on('callSelectedField', function(evnt,obj) { 
        var finalObj = angular.copy($scope.copyObj);
        // $scope.bookingReportParms = Object.assign(finalObj,obj); 
        $scope.bookingReportParms = $.extend(finalObj, obj);
        $scope.callFiterData(true);
    });

    $scope.corpBusinessCalParams = {};
    $scope.indexedTeamsA = [];
    $scope.indexedTeamsB = [];
    
    function filterDate(obj){
        if(obj.trans_date){
            obj.trans_date = new Date(obj.trans_date);
        }
        return obj;
    }

    $scope.filterTeams = function(player) {
        var teamIsNew = indexedTeams.indexOf(player.ta_id) == -1;
        if (teamIsNew) {
            indexedTeams.push(player.ta_id);
        }
        return teamIsNew;
    }

    function returnObj(val){
        return function(element) {
            if(element.ta_id === val.a && element.bkg_src === val.b){
                return element;    
            }
        }
    }

    $scope.bookingListData = function(a,b){
        var objData = {'a':a,'b':b};
        var finalData = $scope.bookingList.filter(returnObj(objData));
        if(finalData.length){
            return finalData[0].olist;
        }
    }

    $scope.checkValidDate = function(fromDate,toDate){
        if(toDate && fromDate > toDate){
            $scope.matureBookByAgentCarsOnly.toDate = fromDate;
        }
    }

    function getDateToSelect(obj){
        if(obj){
            if(obj.currentMonth == true){
                return obj;
            }
        }
    }

    // $scope.corpBusinessCalParams.year = new Date().getFullYear();
    $scope.corpBusinessCalendar = function(){
        $scope.corpBusinessCalParams.corpID = ($scope.userDetails)?$scope.userDetails.corpID:'';
        $scope.corpBusinessCalParams.userID = ($scope.userDetails)?$scope.userDetails.userID:'';
        $scope.corpBusinessCalParams.ta_id = ($scope.userDetails)?$scope.userDetails.ta_id:'';
        
        $scope.corpBusinessListLoading = true;

        $http({
            url: SETTINGS[GLOBALS.ENV].apiUrl+"Corp_BusinessCalendar/GetCorpBusinessCalendar",
            method: 'POST',
            data: $.param($scope.corpBusinessCalParams),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
            }
        }).success(function(response) { 
            $scope.corpBusinessListLoading = false;
            if(response.ResposeCode == 2){
                if(response.DataList.data.length <= 0){
                    $scope.corpBusinessList = "";
                }else{
                    $scope.corpBusinessList = response.DataList.data.filter(filterDate);
                    var datetoSelect = response.DataList.data.filter(getDateToSelect);

                    if(datetoSelect.length){
                        $scope.reportingPeriod = datetoSelect[0].id;
                        $scope.clearDateDropDownSelectedFlag = true;
                        $scope.dateId = datetoSelect[0].id;
                    }
                }
            }else{
                $scope.corpBusinessList = "";
            }
        });
    };

    $scope.corpBusinessCalendar();

    $scope.createFilterForChild = function(query) {
        var teamIsNew = $scope.indexedTeamsB.indexOf(query.type) == -1;
        if (teamIsNew) {
            $scope.indexedTeamsB.push(query.type);
        }
        return teamIsNew;
    }

    $scope.GroupFilterData =function(obj){
        $scope.indexedTeamsB = [];
        return obj;   
    }

    function checkTheDropdownDate(obj){
        if($filter('date')(obj.fromDate, "dd/MM/yyyy") == $filter('date')($scope.matureBookByAgentCarsOnly.fromDate, "dd/MM/yyyy") && $filter('date')(obj.toDate, "dd/MM/yyyy") == $filter('date')($scope.matureBookByAgentCarsOnly.toDate, "dd/MM/yyyy")){
            $scope.clearDateDropDown = false;
            return "false";
        }else{
            $scope.clearDateDropDown = true;
        }
    }
    
    $scope.callFiterData = function(valid){
        if($scope.corpBusinessList.length){
            var checkDate = $scope.corpBusinessList.filter(checkTheDropdownDate);    
            if(checkDate.length){
                $scope.clearDateDropDown = false;
            }else{
                $scope.clearDateDropDown = true;
                if($scope.matureBookByAgentCarsOnly.fromDate && $scope.matureBookByAgentCarsOnly.toDate){
                    $scope.clearDateDropDownSelectedFlag = false;
                }
            }
            if($scope.clearDateDropDownSelectedFlag){
                $scope.clearDateDropDown = false;
                $scope.ClaimedAccountHotel.$valid = true;
                $scope.corpBusinessList.filter(getFromAndToDate);
                $scope.clearDateDropDownSelectedFlag = false
            }
        }
        if($scope.clearDateDropDown == true){
         $scope.reportingPeriod = "SelectReportingPeriod";
     }
     if($scope.ClaimedAccountHotel.$valid){

        $timeout(function(){
            if(valid){
                $scope.bookingReportParms.dataTableCommon.ExportType = 0;
                // $scope.limitOptions = [10,25,50];
                $scope.limitOptions = [50, 100];
                $scope.query = {};
                // $scope.query.limit = 10;
                $scope.query.limit = 50;
                $scope.query.page = 1;
                $scope.bookingReportParms.dataTableCommon.param.Start = 0;
                $scope.bookingReportParms.dataTableCommon.param.Length = 50;
            }
            if(!$scope.bookingReportParms.dataTableCommon.ExportType){
                $scope.unmatchedPayment = [];
                $scope.bookingReportParms.fromDate = $filter('date')($scope.matureBookByAgentCarsOnly.fromDate, "dd/MM/yyyy");
                $scope.bookingReportParms.toDate = $filter('date')($scope.matureBookByAgentCarsOnly.toDate, "dd/MM/yyyy");
                    // $scope.bookingReportParms.dateSelect = $scope.matureBookByAgentCarsOnly.selectedDate;
                    $scope.unmatchedPaymentLoading = true;
                    $scope.$emit('callSelectedFieldByPage',$scope.bookingReportParms);
                    if($scope.bookingReportParms.pageStart){
                        // $scope.limitOptions = [10,25,50];
                        $scope.limitOptions = [50, 100];
                        $scope.query = {};
                        // $scope.query.limit = 10;
                        $scope.query.limit = 50;
                        $scope.query.page = 1;
                    }
                }
                $http({
                    // url: SETTINGS[GLOBALS.ENV].apiUrl+"Payment/GetPaymentClaimed",
                    url: SETTINGS[GLOBALS.ENV].apiUrl+"Payment/GetPaymentClaimedByAgent",
                    method: 'POST',
                    data: $scope.bookingReportParms,
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
                    }
                }).success(function(response) {

                    $scope.unmatchedPaymentLoading = false;

                    if(response.ResposeCode == 2){
                        if(response.DataList.data){
                            $scope.unmatchedPayment = response.DataList.data;
                            $scope.unmatchedPaymentTotal = response.DataList.recordsTotal;
                            $scope.GroupTotal = response.GroupTotal;
                            /*===== For Fixed header =====*/
                            $timeout(function () {

                                if ($("md-table-container .md-table:first").length > 0) {
                                    $("md-table-container .md-table:first").floatThead('destroy');
                                    $("md-table-container .md-table:first").floatThead({ scrollingTop: 60 });
                                }

                            }, 2);

                        }
                        if(response.DataList.fileName){
                            /*window.open(
                                SETTINGS[GLOBALS.ENV].apiUrl+response.DataList.fileName,
                                '_blank'
                                );*/
                                window.open(
                                    response.DataList.fileName,
                                    '_blank'
                                    );
                            }
                        // $scope.unmatchedPaymentTotal = response.DataList.recordsTotal;
                    }else{
                        $scope.unmatchedPayment = "";
                    }
                    if($scope.bookingReportParms.pageStart){
                        delete $scope.bookingReportParms.pageStart;
                    }
                }).error(function(err,status) {
                 /*===== For Fixed header =====*/
                 var $table = $('.md-table:first');
                 $table.floatThead({
                  responsiveContainer: function($table){
                      return $table.closest('.full-height');
                  }

              });
                 $scope.errorView(err,status);
             });
            },300);
}     
}

$scope.totalAvailableCommsion = function(obj){
    var total = {};
    total.commission = 0;
    total.available = 0;
    for(var i = 0; i<obj.length;i++){
        if(obj[i].available){
            total.available =  Number(total.available) + Number(obj[i].available); 
        }
        if(obj[i].commission){
            total.commission =  Number(total.commission) + Number(obj[i].commission); 
        }
    }
    return total;
}

function getFromAndToDate(obj){
    /*if(obj.id == $scope.dateId){
        var offset = new Date().getTimezoneOffset()*60*1000;
        var browserName = navigator.vendor;
        if(browserName == "Google Inc." || browserName == "Apple Computer, Inc."){
            $scope.matureBookByAgentCarsOnly.fromDate = new Date(Date.parse(obj.fromDate)+offset);
            $scope.matureBookByAgentCarsOnly.toDate = new Date(Date.parse(obj.toDate)+offset);
        }else{
            $scope.matureBookByAgentCarsOnly.fromDate = new Date(obj.fromDate);
            $scope.matureBookByAgentCarsOnly.toDate = new Date(obj.toDate);
        }
        $scope.ClaimedAccountHotel.$valid = true;
        $scope.callFiterData(true);
    }*/
    if(obj.id == $scope.dateId){
        var d = new Date();
        var tzName = d.toLocaleString('en', {timeZoneName:'short'}).split(' ').pop();
        
        var offset = new Date().getTimezoneOffset()*60*1000;
        
        var browserName = navigator.vendor;
        if(browserName == "Google Inc." || browserName == "Apple Computer, Inc."){
            var fdt = $filter('date')(obj.fromDate, "MM/dd/yyyy");
            var tdt = $filter('date')(obj.toDate, "MM/dd/yyyy");
            if(tzName == "GMT+5:30"){
                /*$scope.matureBookByAgentCarsOnly.fromDate = new Date(Date.parse(obj.fromDate)+offset);
                $scope.matureBookByAgentCarsOnly.toDate = new Date(Date.parse(obj.toDate)+offset);    */
                $scope.matureBookByAgentCarsOnly.fromDate = new Date(obj.fromDate);
                $scope.matureBookByAgentCarsOnly.toDate = new Date(obj.toDate);
            }else{
                $scope.matureBookByAgentCarsOnly.fromDate = new Date(Date.parse(fdt)+offset);
                $scope.matureBookByAgentCarsOnly.toDate = new Date(Date.parse(tdt)+offset);
            }

        }else{
            $scope.matureBookByAgentCarsOnly.fromDate = new Date(obj.fromDate);
            $scope.matureBookByAgentCarsOnly.toDate = new Date(obj.toDate);
        }
        $scope.ClaimedAccountHotel.$valid = true;
        if(!$scope.clearDateDropDownSelectedFlag){
            $scope.callFiterData(true);
        }

    }

}

$scope.getDates = function(id){
    $scope.clearDateDropDown = false;
    $scope.dateId = id;
    $scope.corpBusinessList.filter(getFromAndToDate);
    // $scope.limitOptions = [10,25,50];
    $scope.limitOptions = [50, 100];
    $scope.query = {};
    // $scope.query.limit = 10;
    $scope.query.limit = 50;
    $scope.query.page = 1;
    $scope.bookingReportParms.dataTableCommon.param.Start = 0;
}

$scope.getPDFLink = function(expot){
    $scope.bookingReportParms.dataTableCommon.ExportType = Number(expot);
    $scope.callFiterData();
}

/* ==== Pagination Click function ==== */
$scope.logOrder = function (order) {
    $scope.bookingReportParms.dataTableCommon.param.Columns = [{}];
    if(order.charAt(0) == '-'){
        $scope.bookingReportParms.dataTableCommon.param.Columns[0].Name = order.slice(1);    
        $scope.bookingReportParms.dataTableCommon.param.Columns[0].Dir = "desc";    
    }else{
        $scope.bookingReportParms.dataTableCommon.param.Columns[0].Name = order;    
        $scope.bookingReportParms.dataTableCommon.param.Columns[0].Dir = "asc";    
    }
    $scope.bookingReportParms.dataTableCommon.ExportType = 0;
    $scope.callFiterData();
};

$scope.logPagination = function (page, limit) {
    var startPage = (limit * (page - 1));
    $scope.bookingReportParms.dataTableCommon.param.Start = startPage;
    $scope.bookingReportParms.dataTableCommon.param.Length = limit;
    $scope.bookingReportParms.dataTableCommon.ExportType = 0;
    $scope.ClaimedAccountHotel.$valid = true;
    $scope.callFiterData();
}



$scope.calTotal = function(obj,type){
    if($scope.GroupTotal.length){
        $scope.GroupTotal.filter(function(ele){
            if(obj.agent == ele.agent && type == ele.cts_Type){
                obj.subTotalVal = ele.total;
            }
        });
    }
    // return obj.subTotalVal;
}

$scope.callBackMainTotal = function(obj){
    var finalTotal = 0;
    if($scope.GroupTotal.length){
        $scope.GroupTotal.filter(function(ele){
            if(obj.agent == ele.agent){
                finalTotal += ele.total;
            }
        });
        return obj.finalTotal = finalTotal;
    }
}

}]);