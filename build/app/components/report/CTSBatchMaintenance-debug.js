App.controller('CTSBatchMaintenance', ['$scope','authService','MetaInformation', '$cookies','$cookieStore','$state','$stateParams', '$http', 'GLOBALS','SETTINGS', 'toaster','$mdDialog','$rootScope','$q','$timeout','$log','$filter', function($scope, authService, MetaInformation, $cookies, $cookieStore, $state, $stateParams, $http, GLOBALS,SETTINGS,toaster, $mdDialog,$rootScope,$q,$timeout,$log,$filter){

    // $scope.limitOptions = [10,25,50];
    $scope.limitOptions = [50, 100];
    $scope.query = {};
    // $scope.query.limit = 10;
    $scope.query.limit = 50;
    $scope.query.page = 1;
    $scope.seo = {
        "seo_title": "Home page",
        "seo_description": "Home page description",
        "seo_keywords": "Home page Keywords"
    };
    setMetaData($scope, $rootScope, MetaInformation, $scope.seo);
    $scope.batchListLoading = false;
    $scope.statusListLoading = false;
    $scope.stateName = '';
    $rootScope.batchReleaseViewRecordRes = "";
    $rootScope.batchReleaseViewRecordResTotal = "";


    var fdate = new Date();
    var tdate = new Date();
    /*var toDate = ((date.getMonth() + 1)) + "/" + (date.gefDate()) + "/" + date.getFullYear();
    var fromDate = ((date.getMonth() + 1) - 6) + "/" + (date.gefDate()) + "/" + date.getFullYear();*/

    var fmonth = (fdate.getMonth()+1)-6;
    var tmonth = (tdate.getMonth()+1);
    fdate.setMonth(fmonth);
    var toDate = tdate.getDate() + "/" + tmonth + "/" + tdate.getFullYear();
    var fromDate = fdate.getDate() + "/" + fmonth + "/" + fdate.getFullYear();

    $scope.batchDataParam = {
        dataTableCommon:
        {param:
            {Draw:1,Start:0,Length:10},
            ExportType:0
        },
        corpID:$scope.userDetails.corpID,
        // fromDate: fromDate,
        // toDate:toDate
        fromDate: "",
        toDate:""
    };

    // {batch_id:543748,corpID:1,unit:null,dataTableCommon:{param:{Draw:1,Start:0,Length:500},ExportType:0}}

    $scope.viewBatchParams = {
        dataTableCommon:
        {param:
            {Draw:1,Start:0,Length:500},
            ExportType:0
        },
        batch_id:"",
        // corpID:1
        corpID:$scope.userDetails.corpID,
        unit:null
    };
    
    $scope.statusParam = {
        // corpID:"",
        corpID:$scope.userDetails.corpID,
        fromDate: "",
        toDate: ""
    };    

    $scope.claimingObj = {
        dataTableCommon: {
            param: {
                Draw: 1,
                Start: 0,
                // Length: 10
                Length: 50
            },
            ExportType: 0
        },
        corpID: ($scope.userDetails && $scope.userDetails.corpID)?$scope.userDetails.corpID:'',
        unit: ($scope.userDetails.unit)?$scope.userDetails.unit: '',
        branch: ($scope.userDetails.branch)?$scope.userDetails.branch: '',
        account: ($scope.userDetails.account)?$scope.userDetails.account: '',
        iata: ($scope.userDetails.iata)?$scope.userDetails.iata: '',
        agent: ($scope.userDetails.agent)?$scope.userDetails.agent: '',
        fromDate: '',
        toDate:'',
        // date: '',
        dateSelect: '',
        propertyBrand: '',
        propertyChain: '',
        propertyName: '',
        propertyAddress: '',
        propertyCity: '',
        propertyState: '',
        propertyZip: '',
        propertyPhone: '',
        propertyPreferred: false,
        lastName: '',
        firstName: '',
        type: '',
        status: '',
        claimed: '0',
        amount: '',
        amountSign: '',
        amountDollar: '0',
        checkNo: '',
        confirmation: '',
        recordNo: '',
        /*bookingNo: null,
        paymentNo: null,*/
        recordSelect: '',
        bookingSource: '',
        accountGroup: '',
        pnr: '',
        rateCode: '',
        vendor: '',
        unclaimed:true,
        recordSelect:'111'
    };

    
    /* ===== CallBack Global Left panel View ===== */
    $scope.copyObj = angular.copy($scope.claimingObj);
    $scope.$emit('callSelectedFieldByPage',$scope.batchDataParam);

    $scope.$on('callSelectedField', function(evnt,obj) { 
        var finalObj = angular.copy($scope.copyObj);
        // $scope.batchDataParam = Object.assign(finalObj,obj); 
        // $scope.GetBatchList(true);
    });

    
    function filterDate(obj){
        var d = new Date();
        var tzName = d.toLocaleString('en', {timeZoneName:'short'}).split(' ').pop();
        var offset = new Date().getTimezoneOffset()*60*1000;
        
        var browserName = navigator.vendor;
        if(browserName == "Google Inc." || browserName == "Apple Computer, Inc."){
            fdt = $filter('date')(obj.fromDate, "MM/dd/yyyy");
            tdt = $filter('date')(obj.toDate, "MM/dd/yyyy");
            if(tzName == "GMT+5:30"){
                /*obj.fromDate = new Date(Date.parse(obj.fromDate)+offset);
                obj.toDate = new Date(Date.parse(obj.toDate)+offset); */
                obj.fromDate = new Date(obj.fromDate);
                obj.toDate = new Date(obj.toDate);    
            }else{
                obj.fromDate = new Date(Date.parse(fdt)+offset);
                obj.toDate = new Date(Date.parse(tdt)+offset);
            }

        }else{
            obj.fromDate = new Date(obj.fromDate);
            obj.toDate = new Date(obj.toDate);
        }

        /*if(obj.deposit_date){
            obj.deposit_date = new Date(obj.deposit_date);
        }
        if(obj.fromDate){
            obj.fromDate = new Date(obj.fromDate);
        }
        if(obj.toDate){
            obj.toDate = new Date(obj.toDate);
        }*/
        return obj;
    }

    function getFormatedDates(obj){
        if(obj.changeDate){
            obj.changeDate = new Date(obj.changeDate);
        }
        return obj;
    }

    $scope.GetBatchData = function(){
        $scope.batchListLoading = true;

        $http({
            url: SETTINGS[GLOBALS.ENV].apiUrl+"Batch/GetBatchData",
            method: 'POST',
            data: $.param($scope.viewBatchParams),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
            }
        }).success(function(response) { 
            $scope.batchListLoading = false;

            if(response.ResposeCode == 2){
                if(response.DataModel.GetDataForbatch.DataList.data.length <= 0){
                    // $scope.noData = "No data found";
                    $scope.viewBatchDatalist = "";
                }else{
                    // delete $scope.noData;
                    $scope.viewBatchDatalist = response.DataModel.GetDataForbatch.DataList.data.filter(filterDate);
                }

                if(response.DataModel.GetBatchHistory.DataList.data.length <= 0){
                    $scope.viewBatchHistory = "";
                }else{
                    $scope.viewBatchHistory = response.DataModel.GetBatchHistory.DataList.data.filter(getFormatedDates);
                }
            }else{
                $scope.viewBatchDatalist = "";
                // $scope.noData = "No data found";
            }
        });
    };  

    $scope.GetBatchList = function(valid){

        if(valid){
            $scope.batchDataParam.dataTableCommon.ExportType = 0;
            $scope.batchDataParam.dataTableCommon.param.Start = 0;
            // $scope.batchDataParam.dataTableCommon.param.Length = 10;
            $scope.batchDataParam.dataTableCommon.param.Length = 50;
            if($scope.batchDataParam.dataTableCommon.param.Columns){
                delete $scope.batchDataParam.dataTableCommon.param.Columns;
            }
        }
        if(!$scope.batchDataParam.dataTableCommon.ExportType){
            $scope.batchListLoading = true;
            $scope.$emit('callSelectedFieldByPage',$scope.batchDataParam);
            if($scope.batchDataParam.pageStart){
                // $scope.limitOptions = [10,25,50];
                $scope.limitOptions = [50, 100];
                $scope.query = {};
                // $scope.query.limit = 10;
                $scope.query.limit = 50;
                $scope.query.page = 1;
            }
        }

        $http({
            url: SETTINGS[GLOBALS.ENV].apiUrl+"Batch/GetBatchList",
            method: 'POST',
            data: $scope.batchDataParam,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
            }
        }).success(function(response) { 
            $scope.batchListLoading = false;

            if(response.ResposeCode == 2){
                if(response.DataList.data){
                    if(response.DataList.data.length <= 0){
                        $scope.noData = "No data found";
                        $scope.batchDatalist = "";
                    }else{
                        delete $scope.noData;
                        $scope.batchDatalist = response.DataList.data.filter(filterDate);

                        /*===== For Fixed header =====*/
                        $timeout(function () {

                            if ($("md-table-container .md-table:first").length > 0) {
                                $("md-table-container .md-table:first").floatThead('destroy');
                                $("md-table-container .md-table:first").floatThead({ scrollingTop: 60 });
                            }

                        }, 2);

                        $scope.batchDataParam.fromDate = $filter('date')($scope.batchDatalist[0].fromDate, "dd/MM/yyyy");
                        $scope.batchDataParam.toDate = $filter('date')($scope.batchDatalist[0].toDate, "dd/MM/yyyy");;

                        $scope.copyObj = angular.copy($scope.claimingObj);
                        $scope.$emit('callSelectedFieldByPage',$scope.batchDataParam);
                        $scope.bookingListTotal = response.DataList.recordsTotal;

                        $scope.$on('callSelectedField', function(evnt,obj) { 
                            var finalObj = angular.copy($scope.copyObj);
                            // $scope.batchDataParam = Object.assign(finalObj,obj); 
                            $scope.batchDataParam = $.extend(finalObj, obj);
                            $scope.GetBatchList(true);
                        });
                    }
                }    
                if(response.DataList.fileName){
                    /*window.open(
                        SETTINGS[GLOBALS.ENV].apiUrl+response.DataList.fileName,
                        '_blank'
                        );*/
                        window.open(
                            response.DataList.fileName,
                            '_blank'
                            );
                    }
                // $scope.bookingListTotal = response.DataList.recordsTotal;
                
            }else{
                $scope.batchDatalist = "";
                $scope.noData = "No data found";
            }
        }).error(function(err,status) {
            /*===== For Fixed header =====*/
            var $table = $('.md-table:first');
            $table.floatThead({
              responsiveContainer: function($table){
                  return $table.closest('.full-height');
              }

          });
            $scope.errorView(err,status);
        });
    };  


    $scope.GetAllStatus = function(){
        $scope.statusListLoading = true;

        $http({
            url: SETTINGS[GLOBALS.ENV].apiUrl+"Batch/GetBatchStepList",
            method: 'POST',
            data: $.param($scope.statusParam),
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
            }
        }).success(function(response) { 
            $scope.statusListLoading = false;

            if(response.ResposeCode == 2){
                $scope.statusList = response.DataList.data;    
            }else{
                $scope.statusList = "";
            }
        });
    };

    if($state.current.name == 'main.CTSBatchMaintenance'){
        $scope.GetBatchList(true);
        $scope.GetAllStatus();
    }    

    $scope.getSelectedStatus = function(status){
        if(status == "All"){
            delete $scope.batchDataParam.batchStatus;
        }else{
            $scope.batchDataParam.batchStatus = status;
        }    
        $scope.batchDataParam.fromDate = "";
        $scope.batchDataParam.toDate = "";
        $scope.batchDataParam.dataTableCommon.ExportType = 0;
        $scope.GetBatchList();
    }

    /***** get batch data(state changed) *****/
    if($state.current.name == 'main.CTSBatchMaintenancBatchData'){
        $scope.stateName = "viewBatch";
        var batchId = $stateParams.id
        $scope.viewBatchParams.batch_id = batchId;
        $scope.GetBatchData();
    }    


    /*===== Payments filter from data =====*/
    $scope.callFilterData = function(valid){
        $scope.claimingObj.batchNo = $scope.viewBatchDatalist[0].web2_batch.id;

        if(valid){
            $scope.claimingObj.dataTableCommon.ExportType = 0;
        }

        $scope.accountListLoading = true;

        if(!$scope.claimingObj.dataTableCommon.ExportType){
            $scope.bookingList = [];
            $scope.$emit('callSelectedFieldByPage',$scope.claimingObj);
        }

        $timeout(function(){
            $http({
                url: SETTINGS[GLOBALS.ENV].apiUrl+"Payments/GetPaymentsFromFilter",
                method: 'POST',
                data: $scope.claimingObj,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
                }
            }).success(function(response) {

                if(response.ResposeCode == 2){
                    if(response.DataList && response.DataList.data){
                        $scope.bookingList = response.DataList.data.filter(filterDate);
                        $rootScope.batchReleaseViewRecordRes = $scope.bookingList;
                        $scope.bookingListTotal = response.DataList.recordsTotal;
                        $rootScope.batchReleaseViewRecordResTotal = $scope.bookingListTotal;
                        $rootScope.batchReleaseViewRecordBatchNo = $scope.claimingObj.batchNo;
                        $state.go('main.quickFindView');
                        // $state.go('main.home');

                    }
                    if(response.DataList.fileName){
                        window.open(
                            SETTINGS[GLOBALS.ENV].apiUrl+response.DataList.fileName,
                            '_blank'
                            );
                    }
                    // $scope.bookingListTotal = response.DataList.recordsTotal;
                }
            }).error(function(err,status) {
                $scope.errorView(err,status);
            });
        },300);
    }

    $scope.getPDFLink = function(expot){
        $scope.batchDataParam.dataTableCommon.ExportType = Number(expot);
        if($scope.batchDataParam.dataTableCommon.param.Columns){
            delete $scope.batchDataParam.dataTableCommon.param.Columns;
        }
        $scope.GetBatchList();
    }

    $scope.back = function(){
        $state.go("main.CTSBatchMaintenance");
        $scope.stateName = '';        
    }

    /* ==== Pagination Click function ==== */
    $scope.logOrder = function (order) {
     $scope.batchDataParam.dataTableCommon.param.Columns = [{}];
     if(order.charAt(0) == '-'){
        $scope.batchDataParam.dataTableCommon.param.Columns[0].Name = order.slice(1);    
        $scope.batchDataParam.dataTableCommon.param.Columns[0].Dir = "desc";    
    }else{
        $scope.batchDataParam.dataTableCommon.param.Columns[0].Name = order;    
        $scope.batchDataParam.dataTableCommon.param.Columns[0].Dir = "asc";    
    }
    $scope.batchDataParam.dataTableCommon.ExportType = 0;
    $scope.GetBatchList();

};

$scope.logPagination = function (page, limit) {
    var startPage = (limit * (page - 1));
    $scope.batchDataParam.dataTableCommon.param.Start = startPage;
    $scope.batchDataParam.dataTableCommon.param.Length = limit;
    $scope.batchDataParam.dataTableCommon.ExportType = 0;
    $scope.GetBatchList();
}

}]);