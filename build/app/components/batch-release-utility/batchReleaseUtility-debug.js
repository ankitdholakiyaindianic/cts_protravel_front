App.controller('batchReleaseUtilityCtrl', ['$scope','$state','$stateParams', '$rootScope', 'MetaInformation','$http', 'SETTINGS', 'GLOBALS', 'authService','toaster','$mdDialog','$filter', '$rootScope', '$timeout', function($scope,$state,$stateParams, $rootScope, MetaInformation,$http, SETTINGS, GLOBALS, authService,toaster,$mdDialog,$filter, $rootScope, $timeout) {

    $scope.unmatchedPaymentRpt = {};
    $scope.unmatchedPaymentRpt.fromDate = null;
    $scope.unmatchedPaymentRpt.toDate = null;
    $scope.unmatchedPaymentRpt.selectedDate = 0;

    $scope.noData = "No data found";
    $scope.corpID = $scope.userDetails.corpID;
    $scope.unmatchedPaymentLoading = false;
    $scope.indexedTeamsA = [];
    $scope.indexedTeamsB = [];

    /* ==== pagination options ==== */
    // $scope.limitOptions = [10,25,50];
    $scope.limitOptions = [5];
    $scope.query = {};
    // $scope.query.limit = 10;
    $scope.query.limit = 3;
    $scope.query.page = 1;
    $scope.totalYes = 0;

    var finalTotal = 0;
    $scope.totalCommission = function(obj){
        finalTotal += Number(obj.net_act_comm);
        return finalTotal;
    }
    function filterDate(obj){
        if(obj.deposit_date){
            obj.deposit_date = new Date(obj.deposit_date);
        }
        if(obj.check_date){
            obj.check_date = new Date(obj.check_date);
        }
        return obj;
    }

    $scope.bookingToFilter = function() {
        indexedTeams = [];
        return $scope.unmatchedPayment;
    }


    $scope.filterTeams = function(player) {
        var teamIsNew = indexedTeams.indexOf(player.cts_type) == -1;
        if (teamIsNew) {
            indexedTeams.push(player.cts_type);
        }
        return teamIsNew;
    }

    $scope.bookingReportParms = {
        dataTableCommon:{
            param:{
                Draw:1,
                Start:0,
                Length:3
            },
            ExportType:0
        },
        corpID:$scope.corpID
        // corpID:105
    };

    /* ===== CallBack Global Left panel View ===== */
    $scope.copyObj = angular.copy($scope.bookingReportParms);
    $scope.$emit('callSelectedFieldByPage',$scope.bookingReportParms);
    

    $scope.$on('callSelectedField', function(evnt,obj) { 
        var finalObj = angular.copy($scope.copyObj);
        // $scope.bookingReportParms = Object.assign(finalObj,obj); 
        $scope.bookingReportParms = $.extend(finalObj, obj);
        $scope.callFiterData(true);
    });

    $scope.batchToReleaseParams = {
        dataTableCommon:
        {param:
            {
                Draw:1,
                Start:0,
                Length:10
            },
            ExportType:0
        },
        // ta_id: $scope.userDetails.ta_id,
        // ta_id: "tzelluk",
        date: ""
    }

    $scope.updateBatchStatusParams = {
        DataTableCommon:{param:{Draw:1,Start:0,Length:10},ExportType:0},
        userID: $scope.userDetails.userID,
        ta_id: $scope.userDetails.ta_id
    };

    $rootScope.unmatchedPaymentList = [];

    if($scope.userDetails){
        if($scope.userDetails.allowBatchRelease == true){
            $scope.abcd = true;
        }
    }

    $scope.getDocIdNumber = function(doc_id){
        return parseInt(doc_id);
    }

    $scope.checkValidDate = function(fromDate,toDate){
        if(toDate && fromDate > toDate){
            $scope.unmatchedPaymentRpt.toDate = fromDate;
        }
    }

    function filterDate(obj){
        if(obj.deposit_date){
            obj.deposit_date = new Date(obj.deposit_date);
        }
        return obj;
    }

    function createFilterFor(query) {
        var teamIsNew = $scope.indexedTeamsA.indexOf(query.businessUnit) == -1;
        if (teamIsNew) {
            $scope.indexedTeamsA.push(query.businessUnit);
        }
        return teamIsNew;
    }

    $scope.createFilterForChild = function(query) {
        /*var teamIsNew = $scope.indexedTeamsB.indexOf(query.deposit_date) == -1;
        if (teamIsNew) {
            $scope.indexedTeamsB.push(query.deposit_date);
        }
        return teamIsNew;*/
        
        query.deposit_date = query.deposit_date;
        var teamIsNew = $scope.indexedTeamsB.indexOf(query.deposit_date) == -1;
        if (teamIsNew) {
            $scope.indexedTeamsB.push(query.deposit_date);
        }
        return teamIsNew;

    }

    $scope.GroupFilterData =function(obj){
        $scope.indexedTeamsB = [];
        return obj;   
    }

    $scope.callFiterData = function(valid){
        if(valid){
            $scope.bookingReportParms.dataTableCommon.ExportType = 0;
            $scope.bookingReportParms.dataTableCommon.param.Start = 0;
            $scope.bookingReportParms.dataTableCommon.param.Length = 3;
            if($scope.bookingReportParms.dataTableCommon.param.Columns){
                delete $scope.bookingReportParms.dataTableCommon.param.Columns;
            }
        }
        if(!$scope.bookingReportParms.dataTableCommon.ExportType){
            $scope.unmatchedPayment = [];
            
            $scope.bookingReportParms.fromdate = null;
            $scope.bookingReportParms.todate = null;
            $scope.unmatchedPaymentLoading = true;
            $scope.$emit('callSelectedFieldByPage',$scope.bookingReportParms);
            if($scope.bookingReportParms.pageStart){
                $scope.query = {};
                $scope.query.limit = 3;
                $scope.query.page = 1;
                $scope.totalYes = 0;
            }
        }
        $http({
            url: SETTINGS[GLOBALS.ENV].apiUrl+"Batch/GetBatchItemsToReview",
            method: 'POST',
            data: $scope.bookingReportParms,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
            }
        }).success(function(response) {

            $scope.unmatchedPaymentLoading = false;
            if(response.ResposeCode == 2){
                if(response.DataList.data){
                    $scope.unmatchedPayment = response.DataList.data;
                    $scope.unmatchedPaymentTotal = response.DataList.recordsTotal;

                    /*===== For Fixed header =====*/
                    $timeout(function () {

                        if ($("md-table-container .md-table:first").length > 0) {
                            $("md-table-container .md-table:first").floatThead('destroy');
                            $("md-table-container .md-table:first").floatThead({ scrollingTop: 60 });
                        }

                    }, 2);

                }
                if(response.DataList.fileName){
                    /*window.open(
                        SETTINGS[GLOBALS.ENV].apiUrl+response.DataList.fileName,
                        '_blank'
                        );*/
                        window.open(
                            response.DataList.fileName,
                            '_blank'
                            );
                    }
                // $scope.unmatchedPaymentTotal = response.DataList.recordsTotal;
            }else{
                $scope.unmatchedPayment = [];
            }
            if($scope.bookingReportParms.pageStart){
                delete $scope.bookingReportParms.pageStart;
            }
        }).error(function(err,status) {
           /*===== For Fixed header =====*/
           var $table = $('.md-table:first');
           $table.floatThead({
              responsiveContainer: function($table){
                  return $table.closest('.full-height');
              }

          });
           $scope.errorView(err,status);
       });
    }

    $scope.callFiterData();

    $scope.$on('refreshBatchReleaseUtilityOnRemove', function(evnt,obj) { 
        $scope.callFiterData();
    });

    if($stateParams.fromdate && $stateParams.todate){
        var fromDate = $stateParams.fromdate;
        var toDate = $stateParams.todate;
        var id = $stateParams.id;
        $scope.unmatchedPaymentRpt.fromDate = new Date(fromDate);
        $scope.unmatchedPaymentRpt.toDate = new Date(toDate);
        $scope.bookingReportParms.prop_chain = id;
        $scope.callFiterData(true);        
    }
    $scope.updateBatchStatusParams.batchID = [];
    $scope.getBatchToRelease = function(){

        $http({
            url: SETTINGS[GLOBALS.ENV].apiUrl+"Batch/GetBatchesToRelease",
            method: 'POST',
            data: $scope.batchToReleaseParams,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
            }
        }).success(function(response) {
            if(response.ResposeCode == 2){
                if(response.DataList.data){
                    $scope.batchToRelease = response.DataList.data;
                    if($scope.batchToRelease){
                        $scope.updateBatchStatusParams.batchID = [];
                        for(var i = 0 ; i < $scope.batchToRelease.length ; i++){
                            $scope.updateBatchStatusParams.batchID.push($scope.batchToRelease[i].batch_id); 
                            $rootScope.unmatchedPaymentListTemp = $scope.updateBatchStatusParams.batchID;
                        }    
                    }
                }
            }
        }).error(function(err,status) {
            $scope.errorView(err,status);
        });
    }

    $scope.updateBatchStatus = function(){
        $scope.updateBatchStatusParams.status = 1;
        // $scope.updateBatchStatusParams.batchID = $scope.updateBatchStatusParams.batchID;
        $rootScope.dateToRelease = $scope.releaseDate;
        $scope.updateBatchStatusParams.date =  $filter('date')($scope.releaseDate, "dd/MM/yyyy");
        
        $http({
            url: SETTINGS[GLOBALS.ENV].apiUrl+"Batch/UpdateBatchStatus",
            method: 'POST',
            data: $scope.updateBatchStatusParams,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
            }
        }).success(function(response) {
            if(response.ResposeCode == 2){
                toaster.pop('Success', '', response.ResponseMessage);
                $rootScope.unmatchedPaymentList = $scope.unmatchedPaymentListTemp;
                $scope.callFiterData();
            }else{
                toaster.pop('error', '', response.ResponseMessage);
            }
            $scope.callBackDialog('cancel');
        }).error(function(err,status) {
            $scope.errorView(err,status);
        });
    }


    $scope.totalAvailableCommsion = function(obj){
        var total = {};
        total.net_act_comm = 0;
        for(var i = 0; i<obj.length;i++){
            if(obj[i].net_act_comm){
                total.net_act_comm =  Number(total.net_act_comm) + Number(obj[i].net_act_comm); 
            }
        }
        return total;
    }

    $scope.releaseDateTxt = "Date To Release";

    $scope.showReleasePopup = function(ev, releaseDate, items) {
        $scope.releaseDate = releaseDate;

        $scope.batchToReleaseParams.date = $filter('date')($scope.releaseDate, "dd/MM/yyyy");
        $scope.batchToReleaseParams.ta_id = items.ta_id;
        $scope.getBatchToRelease();

        $mdDialog.show({
            contentElement: '#releaseDatePopup',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose:false,
            fullscreen: false,
            skipHide: true
        }).finally(function() {
        });
    };
    $scope.callBackDialog = function(data){
        $mdDialog.cancel();
    }

    $scope.getPDFLink = function(expot){
        $scope.bookingReportParms.dataTableCommon.ExportType = Number(expot);
        if($scope.bookingReportParms.dataTableCommon.param.Columns){
            delete $scope.bookingReportParms.dataTableCommon.param.Columns;
        }
        $scope.callFiterData();
    }

    /* ==== Pagination Click function ==== */
    $scope.logOrder = function (order) {
     $scope.bookingReportParms.dataTableCommon.param.Columns = [{}];
     if(order.charAt(0) == '-'){
        $scope.bookingReportParms.dataTableCommon.param.Columns[0].Name = order.slice(1);    
        $scope.bookingReportParms.dataTableCommon.param.Columns[0].Dir = "desc";    
    }else{
        $scope.bookingReportParms.dataTableCommon.param.Columns[0].Name = order;    
        $scope.bookingReportParms.dataTableCommon.param.Columns[0].Dir = "asc";    
    }
    $scope.bookingReportParms.dataTableCommon.ExportType = 0;
    $scope.callFiterData();
};

$scope.logPagination = function (page, limit) {
    var startPage = (limit * (page - 1));
    $scope.bookingReportParms.dataTableCommon.param.Start = startPage;
    $scope.bookingReportParms.dataTableCommon.param.Length = limit;
    $scope.bookingReportParms.dataTableCommon.ExportType = 0;
    $scope.callFiterData();
}

}]);

App.filter('myFormat', function() {
    return function(x) {
        var totalYesBIds = 0;
        for(var i = 0 ; i < x.length ; i++){
            if(x[i].booking_id != null){
                totalYesBIds = totalYesBIds + 1;
            }
        }

        return totalYesBIds;
    };
});