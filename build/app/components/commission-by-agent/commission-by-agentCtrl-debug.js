App.controller('commission-by-agentCtrl', ['$scope','$state','$stateParams', '$rootScope', 'MetaInformation','$http', 'SETTINGS', 'GLOBALS', 'authService','toaster','$mdDialog','$filter', '$timeout', function($scope,$state,$stateParams, $rootScope, MetaInformation,$http, SETTINGS, GLOBALS, authService,toaster,$mdDialog,$filter,$timeout) {

    $scope.commissionByAcountObj = {};
    $scope.commissionByAcountObj.fromDate = null;
    $scope.commissionByAcountObj.toDate = null;
    $scope.commissionByAcountObj.selectedDate = 0;
    $scope.setselectedDate = 0;

    $scope.noData = "No data found";
    $scope.corpID = $scope.userDetails.corpID;

    /* ==== pagination options ==== */
    $scope.limitOptions = [10,25,50];
    $scope.query = {};
    $scope.query.limit = 10;
    $scope.query.page = 1;
    
    $scope.bookingReportParms = {
        DataTableCommon:{
            param:{
                Draw:1,
                Start:0,
                Length:10
            },
            ExportType:0
        },
        corpID:$scope.corpID,
        fromDate:($scope.commissionByAcountObj.fromDate) ? $scope.commissionByAcountObj.fromDate : "",
        toDate:($scope.commissionByAcountObj.toDate) ? $scope.commissionByAcountObj.toDate : "",
        group:"",
        description:"",
        accountCode:"",
        active:""
    };

    /* ===== CallBack Global Left panel View ===== */
    $scope.copyObj = angular.copy($scope.bookingReportParms);
    $scope.$emit('callSelectedFieldByPage',$scope.bookingReportParms);

    $scope.$on('callSelectedField', function(evnt,obj) { 
        var finalObj = angular.copy($scope.copyObj);
        // $scope.bookingReportParms = Object.assign(finalObj,obj); 
        $scope.bookingReportParms = $.extend(finalObj, obj);
        $scope.callFiterData(true);
    });


    function createFilterFor(query) {
        var teamIsNew = $scope.indexedTeamsA.indexOf(query.agent) == -1;
        if (teamIsNew) {
            $scope.indexedTeamsA.push(query.agent);
        }
        return teamIsNew;
    }

    function createFilterForChild(query) {
        var teamIsNew = $scope.indexedTeamsB.indexOf(query.cts_type) == -1;
        if (teamIsNew) {
            $scope.indexedTeamsB.push(query.cts_type);
        }
        return teamIsNew;
    }

    function filterDate(obj){
        if(obj.date){
            obj.date = new Date(obj.date);
        }
        if(obj.in_date){
            obj.in_date = new Date(obj.in_date);
        }
        return obj;
    }

    $scope.checkValidDate = function(fromDate,toDate){
        if(toDate && fromDate > toDate){
            $scope.commissionByAcountObj.toDate = fromDate;
        }
    }


    $scope.callFiterData = function(valid){
        // if($stateParams.fromdate && $stateParams.todate || $scope.commisAgt.$valid){
            if($stateParams.id || $scope.commisAgt.$valid){
                $timeout(function(){
                    if(valid){
                        $scope.bookingReportParms.DataTableCommon.ExportType = 0;
                        $scope.bookingReportParms.DataTableCommon.param.Start = 0;
                        $scope.bookingReportParms.DataTableCommon.param.Length = 10;
                        if($scope.bookingReportParms.DataTableCommon.param.Columns){
                            delete $scope.bookingReportParms.DataTableCommon.param.Columns;
                        }
                    }
                    if(!$scope.bookingReportParms.DataTableCommon.ExportType){
                        $scope.accountListLoading = true;
                        $scope.bookingList = [];
                        $scope.parentResults = [];
                        $scope.subChildResults = [];
                        $scope.indexedTeamsA = [];
                        $scope.indexedTeamsB = [];
                        $scope.bookingReportParms.fromDate = $filter('date')($scope.commissionByAcountObj.fromDate, "dd/MM/yyyy");
                        $scope.bookingReportParms.toDate = $filter('date')($scope.commissionByAcountObj.toDate, "dd/MM/yyyy");
                        $scope.bookingReportParms.dateSelect = $scope.commissionByAcountObj.selectedDate;
                        $scope.setselectedDate = $scope.commissionByAcountObj.selectedDate;
                        $scope.$emit('callSelectedFieldByPage',$scope.bookingReportParms);
                        if($scope.bookingReportParms.pageStart){
                            $scope.query.page = 1;
                        }
                    }
                    $http({
                        url: SETTINGS[GLOBALS.ENV].apiUrl+"Commission/GetCommissionByAgent",
                        method: 'POST',
                        data: $scope.bookingReportParms,
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
                        }
                    }).success(function(response) {
                        $scope.accountListLoading = false;
                        if(response.ResposeCode == 2){
                            if(response.DataList.data){
                                $scope.bookingList = response.DataList.data.filter(filterDate);
                                $scope.parentResults = $scope.bookingList.filter(createFilterFor);
                                $scope.subChildResults = $scope.bookingList.filter(createFilterForChild);
                            }
                            if(response.DataList.fileName){
                                /*window.open(
                                    SETTINGS[GLOBALS.ENV].apiUrl+response.DataList.fileName,
                                    '_blank'
                                    );*/
                                    window.open(
                                        response.DataList.fileName,
                                        '_blank'
                                        );
                                }
                                $scope.bookingListTotal = response.DataList.recordsTotal;
                                $scope.totalPageRecords = response.DataList.recordsFiltered;
                            }else{
                                $scope.bookingList = [];
                            }
                            if($scope.bookingReportParms.pageStart){
                                delete $scope.bookingReportParms.pageStart;
                            }
                        }).error(function(err,status) {
                            $scope.errorView(err,status);
                        });
                    },300);
            }        
        }

        $scope.totalAvailableCommsion = function(obj){
            var total = {};
            total.netRevenue = 0;
            total.netCommission = 0;
            total.commissionPct = 0

            for(var i = 0; i<obj.length;i++){
                if(obj[i].netRevenue){
                    total.netRevenue =  Number(total.netRevenue) + Number(obj[i].netRevenue); 
                }
                if(obj[i].netCommission){
                    total.netCommission =  Number(total.netCommission) + Number(obj[i].netCommission); 
                }
                if(obj[i].commissionPct){
                    total.commissionPct =  Number(total.commissionPct) + Number(obj[i].commissionPct); 
                }
            }
            return total;
        }

        if(localStorage.getItem('filterDetailsFlag')){
            localStorage.removeItem('filterDetails')
            localStorage.removeItem('filterDetailsFlag')
        }else{
            localStorage.setItem('filterDetailsFlag', true);
        }

        if($stateParams.fromdate && $stateParams.todate){
        // if($stateParams.id){
            var fromDate = $stateParams.fromdate;
            var toDate = $stateParams.todate;
            var id = $stateParams.id;
        // $scope.commissionByAcountObj.fromDate = new Date(fromDate);
        // $scope.commissionByAcountObj.toDate = new Date(toDate);
        $scope.bookingReportParms.agent = id;
        $scope.bookingReportParms.type = "1";

        if(localStorage.getItem('filterDetails')){
            $scope.filterDetails = JSON.parse(localStorage.getItem('filterDetails'));

            if($scope.filterDetails && $scope.filterDetails.type){
                $scope.bookingReportParms.type = $scope.filterDetails.type;
            }
            if($scope.filterDetails && $scope.filterDetails.unit){
                $scope.bookingReportParms.unit = $scope.filterDetails.unit;
            }
            if($scope.filterDetails && $scope.filterDetails.dateSelect){
                $scope.bookingReportParms.dateSelect = $scope.filterDetails.dateSelect;
            }
            if($scope.filterDetails && $scope.filterDetails.selectedDate){
                $scope.bookingReportParms.selectedDate = $scope.filterDetails.selectedDate;
            }
            if($scope.filterDetails && $scope.filterDetails.status){
                $scope.bookingReportParms.status = $scope.filterDetails.status;
            }
            if($scope.filterDetails && $scope.filterDetails.fromDate){
                var fDate = $scope.filterDetails.fromDate.split('/');
                $scope.filterDetails.fromDate = fDate[1] + "/" + fDate[0] + "/" + fDate[2];
                $scope.commissionByAcountObj.fromDate = new Date($scope.filterDetails.fromDate);

            }
            if($scope.filterDetails && $scope.filterDetails.toDate){
                var tDate = $scope.filterDetails.toDate.split('/');
                $scope.filterDetails.toDate = tDate[1] + "/" + tDate[0] + "/" + tDate[2];
                $scope.commissionByAcountObj.toDate = new Date($scope.filterDetails.toDate);
            }
            if($scope.filterDetails && $scope.filterDetails.branch){
                $scope.bookingReportParms.branch = $scope.filterDetails.branch;
            }
            if($scope.filterDetails && $scope.filterDetails.account){
                $scope.bookingReportParms.account = $scope.filterDetails.account;
            }
            if($scope.filterDetails && $scope.filterDetails.agent){
                $scope.bookingReportParms.agent = $scope.filterDetails.agent;
            }
            if($scope.filterDetails && $scope.filterDetails.iata){
                $scope.bookingReportParms.iata = $scope.filterDetails.iata;
            }
            if($scope.filterDetails && $scope.filterDetails.propertyBrand){
                $scope.bookingReportParms.propertyBrand = $scope.filterDetails.propertyBrand;
            }
            if($scope.filterDetails && $scope.filterDetails.propertyChain){
                $scope.bookingReportParms.propertyChain = $scope.filterDetails.propertyChain;
            }
            if($scope.filterDetails && $scope.filterDetails.propertyName){
                $scope.bookingReportParms.propertyName = $scope.filterDetails.propertyName;
            }
            if($scope.filterDetails && $scope.filterDetails.propertyAddress){
                $scope.bookingReportParms.propertyAddress = $scope.filterDetails.propertyAddress;
            }
            if($scope.filterDetails && $scope.filterDetails.propertyCity){
                $scope.bookingReportParms.propertyCity = $scope.filterDetails.propertyCity;
            }
            if($scope.filterDetails && $scope.filterDetails.propertyState){
                $scope.bookingReportParms.propertyState = $scope.filterDetails.propertyState;
            }
            if($scope.filterDetails && $scope.filterDetails.propertyZip){
                $scope.bookingReportParms.propertyZip = $scope.filterDetails.propertyZip;
            }
            if($scope.filterDetails && $scope.filterDetails.propertyPhone){
                $scope.bookingReportParms.propertyPhone = $scope.filterDetails.propertyPhone;
            }
            if($scope.filterDetails && $scope.filterDetails.propertyPreferred){
                $scope.bookingReportParms.propertyPreferred = $scope.filterDetails.propertyPreferred;
            }
            if($scope.filterDetails && $scope.filterDetails.accountGrp){
                $scope.bookingReportParms.accountGrp = $scope.filterDetails.accountGrp;
            }
            if($scope.filterDetails && $scope.filterDetails.rateCode){
                $scope.bookingReportParms.rateCode = $scope.filterDetails.rateCode;
            }
        }


        $scope.callFiterData(true);        
    }

    $scope.getPDFLink = function(expot){
        $scope.bookingReportParms.DataTableCommon.ExportType = Number(expot);
        if($scope.bookingReportParms.DataTableCommon.param.Columns){
            delete $scope.bookingReportParms.DataTableCommon.param.Columns;
        }
        $scope.callFiterData();
    }

    /* ==== Pagination Click function ==== */
    $scope.logOrder = function (order) {
        $scope.bookingReportParms.DataTableCommon.param.Columns = [{}];
        if(order.charAt(0) == '-'){
            $scope.bookingReportParms.DataTableCommon.param.Columns[0].Name = order.slice(1);    
            $scope.bookingReportParms.DataTableCommon.param.Columns[0].Dir = "desc";    
        }else{
            $scope.bookingReportParms.DataTableCommon.param.Columns[0].Name = order;    
            $scope.bookingReportParms.DataTableCommon.param.Columns[0].Dir = "asc";    
        }
        $scope.bookingReportParms.DataTableCommon.ExportType = 0;
        $scope.callFiterData();
    };

    $scope.logPagination = function (page, limit) {
        var startPage = (limit * (page - 1));
        $scope.bookingReportParms.DataTableCommon.param.Start = startPage;
        $scope.bookingReportParms.DataTableCommon.param.Length = limit;
        $scope.bookingReportParms.DataTableCommon.ExportType = 0;
        $scope.callFiterData();
    }

}]);
