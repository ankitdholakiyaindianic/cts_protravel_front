App.controller('notProvidedReportCtrl', ['$scope','$state','$stateParams', '$rootScope', 'MetaInformation','$http', 'SETTINGS', 'GLOBALS', 'authService','toaster','$mdDialog','$filter', '$timeout', function($scope,$state,$stateParams, $rootScope, MetaInformation,$http, SETTINGS, GLOBALS, authService,toaster,$mdDialog,$filter, $timeout) {

    $scope.notProvidedRpt = {};
    $scope.notProvidedRpt.fromDate = null;
    $scope.notProvidedRpt.toDate = null;

    $scope.noData = "No data found";
    $scope.corpID = $scope.userDetails.corpID;
    $scope.notProvidedListLoading = false;

    /* ==== pagination options ==== */
    // $scope.limitOptions = [10,25,50];
    $scope.limitOptions = [50, 100];
    $scope.query = {};
    // $scope.query.limit = 10;
    $scope.query.limit = 50;
    $scope.query.page = 1;

    $scope.totalCommission = function(obj){
        var finalTotal = 0;
        for(var i=0;i<obj.length;i++){
            finalTotal += Number(obj[i].Commission);
        }
        return finalTotal;
    }
    function filterDate(obj){
        if(obj.Deposit_Date){
            obj.Deposit_Date = new Date(obj.Deposit_Date);
        }
        if(obj.Arrival_Date){
            obj.Arrival_Date = new Date(obj.Arrival_Date);
        }
        return obj;
    }

    $scope.bookingToFilter = function() {
        indexedTeams = [];
        return notProvidedList;
    }


    $scope.filterTeams = function(player) {
        var teamIsNew = indexedTeams.indexOf(player.businessUnit) == -1;
        if (teamIsNew) {
            indexedTeams.push(player.businessUnit);
        }
        return teamIsNew;
    }

    $scope.bookingReportParms = {
        DataTableCommon:{
            param:{
                Draw:1,
                Start:0,
                Length:$scope.query.limit
            },
            ExportType:0
        },
        corpID:$scope.corpID,
        //corpID:105,
        // fromDate:($scope.notProvidedRpt.fromDate) ? $scope.notProvidedRpt.fromDate : "",
        // toDate:($scope.notProvidedRpt.toDate) ? $scope.notProvidedRpt.toDate : "",
        fromDate: "",
        toDate:"",
        TAID:($scope.userDetails.ta_id) ? $scope.userDetails.ta_id : "",
        dateSelect:"",
        unit:($scope.userDetails.unit) ? $scope.userDetails.unit :"",
        branch:($scope.userDetails.branch) ? $scope.userDetails.branch : "",
        agent:($scope.userDetails.agent) ? $scope.userDetails.agent : "",
        iata:($scope.userDetails.iata) ? $scope.userDetails.iata : "",
        account:($scope.userDetails.account) ? $scope.userDetails.account : "",
        propertyBrand:"",
        propertyChain:"",
        propertyName:"",
        propertyAddress:"",
        propertyCity:"",
        propertyState:"",
        propertyZip:"",
        propertyPhone:"",
        propertyPreferred:"",
        // lastName:"NOTPROVIDED",
        lastName:"notprovided",        
        firstName:"",
        type:"",
        status:"",
        claimed:"0",
        amount:"",
        amountSign:"5",
        amountDollar:"150",
        checkNo:"",
        batchNo:"",
        recordSelect:"",
        bookingSource:"",
        accountGroup:"",
        pnr:"",
        rateCode:""
    };

    /* ===== CallBack Global Left panel View ===== */
    $scope.copyObj = angular.copy($scope.bookingReportParms);
    $scope.$emit('callSelectedFieldByPage',$scope.bookingReportParms);

    $scope.$on('callSelectedField', function(evnt,obj) { 
        var finalObj = angular.copy($scope.copyObj);
        // $scope.bookingReportParms = Object.assign(finalObj,obj); 
        $scope.bookingReportParms = $.extend(finalObj, obj);
        $scope.callFiterData(true);
    });

    $scope.checkValidDate = function(fromDate,toDate){
        if(toDate && fromDate > toDate){
            $scope.notProvidedRpt.toDate = fromDate;
        }
    }

    $scope.callFiterData = function(){
        //$scope.bookingReportParms.fromDate = $filter('date')($scope.notProvidedRpt.fromDate, "dd/MM/yyyy");
        //$scope.bookingReportParms.toDate = $filter('date')($scope.notProvidedRpt.toDate, "dd/MM/yyyy");
        $scope.notProvidedListLoading = true;
        $http({
            url: SETTINGS[GLOBALS.ENV].apiUrl+"Commission/GetNotProvided",
            method: 'POST',
            data: $scope.bookingReportParms,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'bearer '+authService.getAuthToken('AuthToken')
            }
        }).success(function(response) {
            $scope.notProvidedListLoading = false;
            if(response.ResposeCode == 2){
                if(response.DataList.data){
                    $scope.notProvidedList = response.DataList.data.filter(filterDate);
                    $scope.notProvidedListTotal = response.DataList.recordsTotal;
                    $scope.GroupTotal = response.GroupTotal
                    /*===== For Fixed header =====*/
                    $timeout(function () {

                        if ($("md-table-container .md-table:first").length > 0) {
                            $("md-table-container .md-table:first").floatThead('destroy');
                            $("md-table-container .md-table:first").floatThead({ scrollingTop: 60 });
                        }

                    }, 2);

                }
                if(response.DataList.fileName){
                    /*window.open(
                        SETTINGS[GLOBALS.ENV].apiUrl+response.DataList.fileName,
                        '_blank'
                        );*/
                        window.open(
                            response.DataList.fileName,
                            '_blank'
                            );
                    }
                // $scope.notProvidedListTotal = response.DataList.recordsTotal;
            }else{
                $scope.notProvidedList = [];
            }
        }).error(function(err,status) {
         /*===== For Fixed header =====*/
         var $table = $('.md-table:first');
         $table.floatThead({
          responsiveContainer: function($table){
              return $table.closest('.full-height');
          }

      });
         $scope.errorView(err,status);
     });
    }

    $scope.callFiterData();

    $scope.getPDFLink = function(expot){
        $scope.bookingReportParms.DataTableCommon.ExportType = Number(expot);
        $scope.callFiterData();
    }

    /* ==== Pagination Click function ==== */
    $scope.logOrder = function (order) {
      $scope.bookingReportParms.DataTableCommon.param.Columns = [{}];
      if(order.charAt(0) == '-'){
        $scope.bookingReportParms.DataTableCommon.param.Columns[0].Name = order.slice(1);    
        $scope.bookingReportParms.DataTableCommon.param.Columns[0].Dir = "desc";    
    }else{
        $scope.bookingReportParms.DataTableCommon.param.Columns[0].Name = order;    
        $scope.bookingReportParms.DataTableCommon.param.Columns[0].Dir = "asc";    
    }
    $scope.bookingReportParms.DataTableCommon.ExportType = 0;
    $scope.callFilterData();
};

$scope.logPagination = function (page, limit) {
    var startPage = (limit * (page - 1));
    $scope.bookingReportParms.DataTableCommon.param.Start = startPage;
    $scope.bookingReportParms.DataTableCommon.param.Length = limit;
    $scope.bookingReportParms.DataTableCommon.ExportType = 0;
    $scope.callFiterData();
}

function gettotal(data){
    if($scope.bu == data.businessUnit){
        return data.total;
    }
}

$scope.findTotal = function(obj){
    $scope.bu = obj;
    if($scope.GroupTotal.length){
        var ttl = $scope.GroupTotal.filter(gettotal);
        return ttl[0].total;
    }
    
}

}]);