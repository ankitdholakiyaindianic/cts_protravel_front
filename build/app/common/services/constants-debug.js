var app = angular.module('constants', []);
app.constant('GLOBALS', {
    SITE_URL: "/",
    ENV: "DEV", /* DEV, STAGING, PRODUCTION **/
    securityToken : "8080808080808080"
});
app.constant('SETTINGS', {
    DEV: {
        // "apiUrl": "http://10.2.5.25/cst.WebApi/",
        // "apiUrl": "http://10.2.5.25/CTS.WebApi/",
        // "apiUrl": "http://10.2.5.11/cst.WebApi/",
        // "apiUrl": "http://10.2.5.11/CTS.WebApi/",
        // "apiUrl": "http://10.2.5.35/cst.WebApi/",
        // "apiUrl": "http://10.2.5.28/CTS.WebApi/",
        // "apiUrl": "http://10.2.6.32/cst.WebApi/",
        // "apiUrl":"http://cstwebapi-dev.us-west-2.elasticbeanstalk.com/",
        // "apiUrl":"http://cstwebapi-dev.us-east-1.elasticbeanstalk.com/", /* ----- Live Site ----- */
        // "apiUrl": "http://10.2.5.12/cst.WebApi/",
        // "apiUrl":"http://cts-protravel-dev.us-east-1.elasticbeanstalk.com/",
        // "apiUrl":":http//protravel-staging.us-east-1.elasticbeanstalk.com/", /* ----- Staging Site http ----- */
        "apiUrl":"https://protravel-staging.us-east-1.elasticbeanstalk.com/", /* ----- Staging Site https ----- */

        // "apiUrl":"https://api.cts-protravel.com/", /* ----- Production Site https ----- */
        // "apiUrl":"https://api.protravel-staging.com/", /* ----- Staging Site https ----- */
        
        // "apiUrl":"http://protravel-production.us-east-1.elasticbeanstalk.com/", /* ----- Production Site ----- */
        //"apiUrl":"https://protravel-production.us-east-1.elasticbeanstalk.com/", /* ----- Production Site ----- */
        // "apiUrl":"http://protravel-timeout.us-east-1.elasticbeanstalk.com/",
        // "siteUrl":"src/",
        "siteUrl":"build/"
    },
    STAGING: {
        // "apiUrl": "http://ec2-52-91-103-72.compute-1.amazonaws.com/ctsapi/",
        "apiUrl": "http://ec2-52-91-103-72.compute-1.amazonaws.com/ctsApi/",
        //"apiUrl": "https://ec2-52-91-103-72.compute-1.amazonaws.com/ctsApi/",
        //"apiUrl": "http://ec2-54-147-47-230.compute-1.amazonaws.com/ctsApi/",
        "siteUrl":"build/"
    },
    PRODUCTION: {
       "apiUrl": "http://windev.indianic.com/cts/",
        //"apiUrl": "https://windev.indianic.com/cts/",
        "siteUrl":"build/"
    }
});                                             